CONFIGURATION = Release
OUTPUTS := bin obj Output HatchAnimEditor

all: build output

clean:
	@$(foreach out, $(OUTPUTS), rm -rf Build/$(out))

remake: clean all

build:
	@cd Source ; \
	msbuild /nologo /verbosity:minimal -p:Configuration=$(CONFIGURATION) HatchAnimEditor.sln; \
	cd ..

output:
	@rm -rf Build/Output/
	@mkdir Build/Output/
	@cp MonoAnimEditor.sh Build/HatchAnimEditor
	@chmod +x Build/HatchAnimEditor
	@yes | cp Build/bin/$(CONFIGURATION)/* Build/Output/
	@rm -rf Build/bin/$(CONFIGURATION)/*

run:
	@cd Build && ./HatchAnimEditor
