﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RSDKv5 {
    // Thanks to Xeeynamo for the Animation information
    [Serializable]
    public class Animation {

        public const uint Sig = 0x00525053;
        public int FrameCount = 0;
        public List<string> SpriteSheets = new List<string>();
        public List<string> CollisionBoxes = new List<string>();
        public List<AnimationEntry> Animations = new List<AnimationEntry>();

#if DEBUG
        public static bool Print = true;
#else
        public static bool Print = false;
#endif

        class RSDKEndian {
            public static string Read(Stream stream) {
                string str = "";
                int length = stream.ReadByte();
                if (length == -1) {
                    throw new EndOfStreamException();
                }
                for (int i = 0; i < length; i++) {
                    char it = (char)stream.ReadByte();
                    if (it > 0)
                        str += it;
                }
                return str;
            }

            public static void Write(Stream stream, string str) {
                stream.WriteByte((byte)(str.Length + 1));
                stream.Write(Encoding.Default.GetBytes(str), 0, str.Length);
                stream.WriteByte(0);
            }
        }

        public void Load(Reader reader) {
            if (reader.ReadUInt32() != Sig)
                throw new Exception("Invalid Signature!");
            FrameCount = reader.ReadInt32();
            int spriteSheetCount = reader.ReadByte();

            if (Print) {
                Console.WriteLine("FrameCount: " + FrameCount);
                Console.WriteLine("Sprite Sheets (Count: " + spriteSheetCount + ")");
            }

            for (int i = 0; i < spriteSheetCount; ++i) {
                string sheet = reader.ReadRSDKString();
                SpriteSheets.Add(sheet);

                if (Print) {
                    Console.WriteLine(" - " + sheet);
                }
            }

            int collisionBoxCount = reader.ReadByte();
            if (Print) {
                Console.WriteLine("Hitboxes (Count: " + collisionBoxCount + ")");
            }

            for (int i = 0; i < collisionBoxCount; ++i) {
                string sheet = reader.ReadRSDKString();
                CollisionBoxes.Add(sheet);

                if (Print) {
                    Console.WriteLine(" - " + sheet);
                }
            }

            var animationCount = reader.ReadInt16();
            if (Print) {
                Console.WriteLine("Animation Entries (Count: " + animationCount + ")");
            }

            for (int i = 0; i < animationCount; ++i)
                Animations.Add(new AnimationEntry(reader, this));
        }

        public void Save(Writer writer) {
            writer.Write(Sig);

            FrameCount = 0;
            for (int i = 0; i < Animations.Count; ++i)
                FrameCount += Animations[i].Frames.Count;

            writer.Write(FrameCount);
            writer.Write((byte)SpriteSheets.Count);
            for (int i = 0; i < SpriteSheets.Count; ++i)
                writer.WriteRSDKString(SpriteSheets[i]);

            writer.Write((byte)CollisionBoxes.Count);
            for (int i = 0; i < CollisionBoxes.Count; ++i)
                writer.WriteRSDKString(CollisionBoxes[i]);

            writer.Write((ushort)Animations.Count);
            for (int i = 0; i < Animations.Count; ++i)
                Animations[i].Write(writer, this);
        }

        public class AnimationEntry {
            public string AnimName;
            public List<Frame> Frames = new List<Frame>();
            public int FrameLoop;
            public int FrameSpeed;
            public byte Unknown;

            public AnimationEntry(string animName) {
                AnimName = animName;
                FrameLoop = 0;
                FrameSpeed = 0;
            }

            public AnimationEntry(BinaryReader reader, Animation anim) {
                Read(reader, anim);
            }

            public AnimationEntry Read(BinaryReader reader, Animation anim) {
                AnimName = RSDKEndian.Read(reader.BaseStream);
                short frameCount = reader.ReadInt16();
                FrameSpeed = reader.ReadInt16();
                FrameLoop = reader.ReadByte();
                Unknown = reader.ReadByte();

                if (Print) {
                    Console.WriteLine("- " + AnimName + " (Frame Count: " + frameCount + ", Speed: " + FrameSpeed + ", Loop Index: " + FrameLoop + ")");
                    Console.WriteLine("Pos: " + reader.BaseStream.Position);
                }

                for (int i = 0; i < frameCount; ++i) {
                    Frames.Add(Frame.ReadFrame(reader, anim));
                }
                return this;
            }

            public AnimationEntry Write(Writer writer, Animation anim) {
                writer.WriteRSDKString(AnimName);
                writer.Write((ushort)Frames.Count);
                writer.Write((ushort)FrameSpeed);
                writer.Write((byte)FrameLoop);
                writer.Write((byte)Unknown);
                for (int i = 0; i < Frames.Count; ++i) {
                    Frame.WriteFrame(writer, anim, this, i);
                }
                return this;
            }
        }

        public class Frame {
            public List<HitBox> HitBoxes = new List<HitBox>();
            public int SpriteSheet = 0;
            public int CollisionBox = 0;
            public int Duration = 0;
            public int ID = 0;
            public int X = 0;
            public int Y = 0;
            public int Width = 0;
            public int Height = 0;
            public int CenterX = 0;
            public int CenterY = 0;
            public Animation Anim = null;

            public Frame(int width, int height) {
                Width = width;
                Height = height;
            }

            public static Frame ReadFrame(BinaryReader reader, Animation anim) {
                var frame = new Frame(0, 0);
                frame.SpriteSheet = reader.ReadByte();
                frame.CollisionBox = 0;
                frame.Duration = reader.ReadInt16();
                frame.ID = reader.ReadInt16();
                frame.X = reader.ReadInt16();
                frame.Y = reader.ReadInt16();
                frame.Width = reader.ReadInt16();
                frame.Height = reader.ReadInt16();
                frame.CenterX = reader.ReadInt16();
                frame.CenterY = reader.ReadInt16();
                frame.Anim = anim;

                for (int i = 0; i < anim.CollisionBoxes.Count; ++i) {
                    var hitBox = new HitBox();
                    hitBox.Left = reader.ReadInt16();
                    hitBox.Top = reader.ReadInt16();
                    hitBox.Right = reader.ReadInt16();
                    hitBox.Bottom = reader.ReadInt16();
                    frame.HitBoxes.Add(hitBox);
                }
                return frame;
            }

            public static Frame WriteFrame(Writer writer, Animation anim, AnimationEntry anime, int index) {
                var frame = anime.Frames[index];
                writer.Write((byte)frame.SpriteSheet);
                // frame.CollisionBox = 0;
                writer.Write((ushort)frame.Duration);
                writer.Write((ushort)frame.ID);
                writer.Write((ushort)frame.X);
                writer.Write((ushort)frame.Y);
                writer.Write((ushort)frame.Width);
                writer.Write((ushort)frame.Height);
                writer.Write((ushort)frame.CenterX);
                writer.Write((ushort)frame.CenterY);

                for (int i = 0; i < anim.CollisionBoxes.Count; ++i) {
                    var hitBox = frame.HitBoxes[i];
                    writer.Write((ushort)hitBox.Left);
                    writer.Write((ushort)hitBox.Top);
                    writer.Write((ushort)hitBox.Right);
                    writer.Write((ushort)hitBox.Bottom);
                }
                return frame;
            }

            public Frame Clone() {
                Frame frame = new Frame(Width, Height);
                frame.SpriteSheet = SpriteSheet;
                frame.CollisionBox = CollisionBox;
                frame.Duration = Duration;
                frame.ID = ID;
                frame.X = X;
                frame.Y = Y;
                frame.CenterX = CenterX;
                frame.CenterY = CenterY;
                frame.Anim = Anim;

                for (int i = 0; i < this.Anim.CollisionBoxes.Count; ++i) {
                    var hitBox = new HitBox();
                    hitBox.Left = this.HitBoxes[i].Left;
                    hitBox.Top = this.HitBoxes[i].Top;
                    hitBox.Right = this.HitBoxes[i].Right;
                    hitBox.Bottom = this.HitBoxes[i].Bottom;
                    frame.HitBoxes.Add(hitBox);
                }
                return frame;
            }

            public class HitBox {
                public int Left = 0, Right = 0, Top = 0, Bottom = 0;
            }

            /// <summary>
            /// Retrieves the PivotX value for the frame relative to its horrizontal flipping.
            /// </summary>
            /// <param name="fliph">Horizontal flip</param>
            public int RelCenterX(bool fliph) {
                return (fliph ? -(Width + CenterX) : CenterX);
            }

            /// <summary>
            /// Retrieves the PivotY value for the frame relative to its vertical flipping.
            /// </summary>
            /// <param name="flipv">Vertical flip</param>
            public int RelCenterY(bool flipv) {
                return (flipv ? -(Height + CenterY) : CenterY);
            }
        }
    }
}