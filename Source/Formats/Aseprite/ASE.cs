﻿using RSDKv5;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace HatchAnimEditor {
    public class ASE {
        public ushort Width;
        public ushort Height;
        public ushort ColorDepth;
        public uint Flags;
        public byte PaletteEntry;
        public ushort NumberOfColors;
        public byte PixelWidth;
        public byte PixelHeight;
        public short GridX;
        public short GridY;
        public ushort GridWidth;
        public ushort GridHeight;

        public int LayerCount;

        public enum ColorProfiles : ushort {
            None = 0,
            sRGB,
            ICC,
        }

        public List<Frame> Frames = new List<Frame>();
        public List<Layer> Layers = new List<Layer>();
        public List<AnimRange> AnimRanges = new List<AnimRange>();
        public uint[] Palette = null;
        public uint ColorCount = 0;

        public ASE() {
            
        }

        public ASE(Stream stream) : this(new Reader(stream)) { }

        internal ASE(Reader reader) {
            uint fileSize = reader.ReadUInt32();
            ushort magicNumber = reader.ReadUInt16();

            Console.WriteLine("magicNumber: 0x" + magicNumber.ToString("X04"));

            ushort frameCount = reader.ReadUInt16();

            Width = reader.ReadUInt16();
            Height = reader.ReadUInt16();
            ColorDepth = reader.ReadUInt16();
            Flags = reader.ReadUInt32();
            reader.ReadUInt16(); // formerly Speed
            reader.ReadUInt32();
            reader.ReadUInt32();
            PaletteEntry = reader.ReadByte();
            reader.Seek(3, SeekOrigin.Current);
            NumberOfColors = reader.ReadUInt16();
            PixelWidth = reader.ReadByte();
            PixelHeight = reader.ReadByte();
            GridX = reader.ReadInt16();
            GridY = reader.ReadInt16();
            GridWidth = reader.ReadUInt16();
            GridHeight = reader.ReadUInt16();

            // Padding
            reader.Seek(84, SeekOrigin.Current);

            LayerCount = 0;

            Console.WriteLine("ColorDepth: " + ColorDepth);
            Console.WriteLine("Flags: " + Flags);
            Console.WriteLine("PaletteEntry: " + PaletteEntry);

            for (int i = 0; i < frameCount; i++) {
                Frames.Add(new Frame(reader, this));
            }
        }

        public void Write(Writer writer) {
            
        }

        public class AnimRange {
            public string Name;
            public int Start;
            public int End;
            public int Type;
            public AnimRange(string name, int start, int end, int type) {
                Name = name;
                Start = start;
                End = end;
                Type = type;
            }
        }

        public class Layer {
            public string Name;
            public int Flags;
            public int Type;
            public int BlendMode;
            public Layer(string name, int flags, int type, int blendMode) {
                Name = name;
                Flags = flags;
                Type = type;
                BlendMode = blendMode;
            }
            public enum FlagTypes {
                Visible = 1,
                Editable = 2,
                MovementLocked = 4,
                Background = 8,
                PreferLinkedCels = 16,
                Collapsed = 32,
                Reference = 64,
            }
        }

        public class Frame {
            public int FrameDuration;
            public List<uint[]> PixelData = new List<uint[]>();

            public Frame(ASE ase) {

            }

            public Frame(Stream stream, ASE ase) : this(new Reader(stream), ase) { }

            internal Frame(Reader reader, ASE ase) {
                // PixelData = new uint[width * height];

                uint frameSize = reader.ReadUInt32();
                ushort magicNumber = reader.ReadUInt16();
                // Console.WriteLine("magicNumber: 0x" + magicNumber.ToString("X04"));

                ushort oldChunkCount = reader.ReadUInt16();
                FrameDuration = reader.ReadUInt16();
                reader.ReadUInt16();
                uint newChunkCount = reader.ReadUInt32();

                uint chunkCount = newChunkCount;
                if (chunkCount == 0)
                    chunkCount = oldChunkCount;

                for (int i = 0; i < ase.LayerCount; i++) {
                    PixelData.Add(new uint[ase.Width * ase.Height]);
                }

                // Console.WriteLine("Frame Duration: " + FrameDuration + "ms");

                for (uint i = 0; i < chunkCount; i++) {
                    uint chunkSize = reader.ReadUInt32();
                    ushort chunkType = reader.ReadUInt16();

                    switch (chunkType) {
                        // Old palette chunk
                        case 0x0004:
                        case 0x0011: {
                            if (ase.Palette != null) {
                                Console.WriteLine("Old Palette Chunk: Ignoring...");
                                reader.Seek(chunkSize - 6, SeekOrigin.Current);
                                break;
                            }

                            int ind = 0, total = 0;
                            ushort packetCount = reader.ReadUInt16();
                            Console.WriteLine("Old Palette Chunk: " + packetCount + " packets");
                            for (int p = 0; p < packetCount; p++) {
                                byte paletteEntries = reader.ReadByte();
                                int colorCount = reader.ReadByte();
                                if (colorCount == 0)
                                    colorCount = 256;
                                Console.WriteLine("Entries to Skip: " + paletteEntries);
                                Console.WriteLine("Color Count: " + colorCount);
                                total += colorCount;
                                Array.Resize(ref ase.Palette, total);
                                for (int c = 0; c < colorCount; c++) {
                                    byte r = reader.ReadByte();
                                    byte g = reader.ReadByte();
                                    byte b = reader.ReadByte();
                                    uint argb = (uint)(b << 16 | g << 8 | r) | 0xFF000000;

                                    ase.Palette[ind + c] = argb;

                                    Console.WriteLine("Color #" + c + ": " + argb.ToString("X08"));
                                }
                                ind += colorCount;
                            }
                            break;
                        }
                        // Layer Chunk
                        case 0x2004: {
                            ushort flags = reader.ReadUInt16();
                            ushort layerType = reader.ReadUInt16();
                            ushort layerChildLevel = reader.ReadUInt16();

                            reader.ReadUInt16(); // default layer width (ignored)
                            reader.ReadUInt16(); // default layer height (ignored)

                            ushort blendMode = reader.ReadUInt16();
                            byte opacity = reader.ReadByte();
                            reader.Seek(3, SeekOrigin.Current);

                            string layerName = reader.ReadHeaderedString();
                            Console.WriteLine("Layer Name: " + layerName + " Flags " + flags + " BlendMode " + blendMode + " Opacity " + opacity);

                            ase.Layers.Add(new Layer(layerName, flags, layerType, blendMode));

                            PixelData.Add(new uint[ase.Width * ase.Height]);
                            ase.LayerCount++;
                            break;
                        }
                        // Cel Chunk
                        case 0x2005: {
                            long endPos = reader.Pos + chunkSize - 6;

                            ushort layerIndex = reader.ReadUInt16();
                            short posX = reader.ReadInt16();
                            short posY = reader.ReadInt16();
                            byte opacity = reader.ReadByte();

                            ushort celType = reader.ReadUInt16();
                            reader.Seek(7, SeekOrigin.Current);

                            // Console.Write("Cel Chunk: PosX " + posX + " PosY " + posY + " Type ");

                            switch (celType) {
                                // Raw cel
                                case 0:
                                    ushort cwidth = reader.ReadUInt16();
                                    ushort cheight = reader.ReadUInt16();

                                    // Console.WriteLine("Raw (W: " + cwidth + ", H: " + cheight + ")");

                                    switch (ase.ColorDepth) {
                                        case 8:
                                            for (int y = 0; y < cheight; y++) {
                                                for (int x = 0; x < cwidth; x++) {
                                                    PixelData[layerIndex][posX + x + (posY + y) * ase.Width] = reader.ReadByte();
                                                }
                                            }
                                            break;
                                        case 16:
                                            for (int y = 0; y < cheight; y++) {
                                                for (int x = 0; x < cwidth; x++) {
                                                    PixelData[layerIndex][posX + x + (posY + y) * ase.Width] = reader.ReadUInt16();
                                                }
                                            }
                                            break;
                                        case 32:
                                            for (int y = 0; y < cheight; y++) {
                                                for (int x = 0; x < cwidth; x++) {
                                                    PixelData[layerIndex][posX + x + (posY + y) * ase.Width] = reader.ReadUInt32();
                                                }
                                            }
                                            break;
                                    }
                                    break;
                                // Linked cel
                                case 1:
                                    ushort framePos = reader.ReadUInt16();
                                    // Console.WriteLine("Linked (Frame Pos: " + framePos + ")");
                                    break;
                                // Compressed Image
                                case 2:
                                    ushort cpwidth = reader.ReadUInt16();
                                    ushort cpheight = reader.ReadUInt16();

                                    reader.ReadUInt16();

                                    uint pxSize = (uint)(ase.ColorDepth / 8);

                                    uint compressed_size = (uint)(endPos - reader.Pos);
                                    uint uncompressed_size = pxSize * cpwidth * cpheight;

                                    // Console.WriteLine("Compressed (W: " + cpwidth + ", H: " + cpheight + ", Comp: 0x" + compressed_size.ToString("X") + ", Decomp: 0x" + uncompressed_size.ToString("X") + ")");
                                    
                                    using (MemoryStream outMemoryStream = new MemoryStream())
                                    using (MemoryStream inMemoryStream = new MemoryStream()) {
                                        inMemoryStream.Write(reader.ReadBytes(compressed_size), 0, (int)compressed_size);
                                        inMemoryStream.Position = 0;
                                        
                                        using (DeflateStream decompress = new DeflateStream(inMemoryStream, CompressionMode.Decompress)) {
                                            decompress.CopyTo(outMemoryStream);
                                            outMemoryStream.Position = 0;

                                            using (Reader pxReader = new Reader(outMemoryStream)) {
                                                for (int y = 0; y < cpheight; y++) {
                                                    for (int x = 0; x < cpwidth; x++) {
                                                        uint value = 0;
                                                        switch (ase.ColorDepth) {
                                                            case 8:
                                                                value = pxReader.ReadByte();
                                                                break;
                                                            case 16:
                                                                value = pxReader.ReadUInt16();
                                                                break;
                                                            case 32:
                                                                value = pxReader.ReadUInt32();
                                                                break;
                                                        }
                                                        int px = posX + x;
                                                        int py = posY + y;
                                                        if (px >= 0 && py >= 0 && px < ase.Width && py < ase.Height)
                                                            PixelData[layerIndex][px + py * ase.Width] = value;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    Console.WriteLine("UNKNOWN (" + celType + ")");
                                    break;
                            }

                            break;
                        }
                        // Color Profile Chunk
                        case 0x2007: {
                            ushort type = reader.ReadUInt16();
                            ushort flags = reader.ReadUInt16();
                            float fixedGamma = reader.ReadSingle();

                            reader.Seek(8, SeekOrigin.Current);
                            if (type == (ushort)ColorProfiles.ICC) {
                                uint dataLen = reader.ReadUInt32();
                                reader.Seek(dataLen, SeekOrigin.Current);
                            }
                            Console.WriteLine("Color Profile: " + (ColorProfiles)type);
                            break;
                        }
                        // Tags Chunk
                        case 0x2018: {
                            ushort tagCount = reader.ReadUInt16();
                            reader.Seek(8, SeekOrigin.Current);

                            Console.WriteLine("Tags Chunk: " + tagCount + " count");

                            for (ushort t = 0; t < tagCount; t++) {
                                ushort frameStart = reader.ReadUInt16();
                                ushort frameEnd = reader.ReadUInt16();
                                byte loopAnimDirection = reader.ReadByte();
                                // 0 - Forward
                                // 1 - Reverse
                                // 2 - Ping-pong
                                reader.Seek(8, SeekOrigin.Current);

                                byte r = reader.ReadByte();
                                byte g = reader.ReadByte();
                                byte b = reader.ReadByte();
                                reader.ReadByte();

                                string tagName = reader.ReadHeaderedString();
                                switch (loopAnimDirection) {
                                    case 0:
                                        Console.WriteLine("Tag " + tagName + " [" + frameStart + " -> " + frameEnd + "]");
                                        break;
                                    case 1:
                                        Console.WriteLine("Tag " + tagName + " [" + frameStart + " <- " + frameEnd + "]");
                                        break;
                                    case 2:
                                        Console.WriteLine("Tag " + tagName + " [" + frameStart + " <-> " + frameEnd + "]");
                                        break;
                                }

                                ase.AnimRanges.Add(new AnimRange(tagName, frameStart, frameEnd, loopAnimDirection));
                            }
                            break;
                        }
                        // Palette Chunk
                        case 0x2019: {
                            uint paletteSize = reader.ReadUInt32();
                            uint colorIndexFirst = reader.ReadUInt32();
                            uint colorIndexLast = reader.ReadUInt32();
                            reader.Seek(8, SeekOrigin.Current);

                            Console.WriteLine("Palette Chunk (New): " + paletteSize + " palette size (" + colorIndexFirst + " -> " + colorIndexLast + ")");
                            ase.Palette = new uint[paletteSize];

                            for (uint p = colorIndexFirst; p <= colorIndexLast; p++) {
                                ushort flags = reader.ReadUInt16();
                                byte r = reader.ReadByte();
                                byte g = reader.ReadByte();
                                byte b = reader.ReadByte();
                                byte a = reader.ReadByte();
                                uint argb = (uint)(a << 24 | b << 16 | g << 8 | r);

                                ase.Palette[p] = argb;

                                if ((flags & 1) != 0) {
                                    string colorName = reader.ReadHeaderedString();
                                    // Console.WriteLine("Color #" + p + " (" + colorName + "): " + argb.ToString("X08"));
                                }
                                else {
                                    // Console.WriteLine("Color #" + p + ": " + argb.ToString("X08"));
                                }
                            }
                            break;
                        }
                        // Fallback
                        default:
                            Console.WriteLine("Unknown Chunk type: 0x" + chunkType.ToString("X04"));
                            reader.Seek(chunkSize - 6, SeekOrigin.Current);
                            break;
                    }
                }
            }
        }
    }
}
