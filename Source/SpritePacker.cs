﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HatchAnimEditor {
    public class SpritePacker {
        public class Rect {
            public int X;
            public int Y;
            public int Width;
            public int Height;

            public Rect(int x, int y, int width, int height) {
                X = x;
                Y = y;
                Width = width;
                Height = height;
            }
        }

        public class Box {
            public int ID;
            public int PackageID;
            public int OffX;
            public int OffY;
            public Rect Rect;

            public Box(int id, Rect rect) {
                ID = id;
                Rect = rect;
            }
        }

        public class Package {
            public int Width;
            public int Height;
            public List<Box> Boxes = new List<Box>();
        }

        public class PackageNode {
            public Box box;
            public bool used = false;
            public PackageNode right = null;
            public PackageNode bottom = null;

        }

        static PackageNode FindNode(PackageNode root, int width, int height) {
            if (root.used) {
                PackageNode space;

                // Console.WriteLine("FindNode Checking Right");
                space = FindNode(root.right, width, height);
                if (space != null)
                    return space;

                // Console.WriteLine("FindNode Checking Bottom");
                space = FindNode(root.bottom, width, height);
                if (space != null)
                    return space;
            }
            else if (width <= root.box.Rect.Width && height <= root.box.Rect.Height) {
                // Console.WriteLine("FindNode " + width + " x " + height + " root.ID " + root.box.ID);
                return root;
            }
            return null;
        }
        static Box InsertNode(PackageNode parent, int w, int h) {
            parent.used = true;

            // Console.WriteLine("InsertNode (X: " + parent.box.Rect.X + " Y " + parent.box.Rect.Y + " W " + parent.box.Rect.Width + " H " + parent.box.Rect.Height + ")");

            parent.right = new PackageNode();
            parent.right.box = new Box(-1, new Rect(parent.box.Rect.X + w, parent.box.Rect.Y, parent.box.Rect.Width - w, h));

            // Console.WriteLine("InsertNode right (X: " + parent.right.box.Rect.X + " Y " + parent.right.box.Rect.Y + " W " + parent.right.box.Rect.Width + " H " + parent.right.box.Rect.Height + ")");

            parent.bottom = new PackageNode();
            parent.bottom.box = new Box(-1, new Rect(parent.box.Rect.X, parent.box.Rect.Y + h, parent.box.Rect.Width, parent.box.Rect.Height - h));

            // Console.WriteLine("InsertNode bottom (X: " + parent.bottom.box.Rect.X + " Y " + parent.bottom.box.Rect.Y + " W " + parent.bottom.box.Rect.Width + " H " + parent.bottom.box.Rect.Height + ")");

            // Console.WriteLine();

            return parent.box;
        }

        public static List<Box> SortBoxesByID(List<Box> boxes) {
            return boxes.OrderBy(o => o.ID).ToList();
        }
        public static List<Box> SortBoxesByArea(List<Box> boxes) {
            return boxes.OrderBy(o => -(o.Rect.Width * o.Rect.Height)).ToList();
        }
        public static List<Box> SortBoxesByWidth(List<Box> boxes) {
            return boxes.OrderBy(o => -(o.Rect.Width)).ToList();
        }
        public static List<Box> SortBoxesByHeight(List<Box> boxes) {
            return boxes.OrderBy(o => -(o.Rect.Height)).ToList();
        }
        public static List<Box> SortBoxesByMaxSize(List<Box> boxes) {
            return boxes.OrderBy(o => -(o.Rect.Width > o.Rect.Height ? o.Rect.Width : o.Rect.Height)).ToList();
        }

        public static List<Package> PackBoxes(ref List<Box> boxe, bool doResize, int defaultWidth = 64, int defaultHeight = 64, int maxWidth = 2048, int maxHeight = 2048) {
            List<Box> boxes;

            // boxes = SortBoxesByMaxSize(boxe);
            // boxes = SortBoxesByWidth(boxe);
            boxes = SortBoxesByArea(boxe);
            boxes = SortBoxesByHeight(boxe);

            List<Package> packageList = new List<Package>();

            Package package = new Package();
            package.Width = defaultWidth;
            package.Height = defaultHeight;

            PackageNode root = new PackageNode();
            root.box = new Box(-1, new Rect(0, 0, package.Width, package.Height));

            packageList.Add(package);


            bool contin = false;

            bool increaseSide = maxWidth > maxHeight;

            int startBox = 0;

            while (true) {
                contin = false;

                var i = startBox;
                for (; i < boxes.Count; i++) {
                    PackageNode node;
                    if ((node = FindNode(root, boxes[i].Rect.Width, boxes[i].Rect.Height)) != null) {
                        int ID = boxes[i].ID;
                        Box where = InsertNode(node, boxes[i].Rect.Width, boxes[i].Rect.Height);
                        boxes[i].PackageID = packageList.Count - 1;
                        boxes[i].Rect.X = where.Rect.X;
                        boxes[i].Rect.Y = where.Rect.Y;
                    }
                    else {
                        Console.WriteLine(">> Package[" + (packageList.Count - 1) + "].Box[" + i + "] could not fit inside package of size " + package.Width + " x " + package.Height);

                        if (doResize) {
                            if (package.Width >= maxWidth && package.Height >= maxHeight) {
                                package = new Package();
                                package.Width = defaultWidth;
                                package.Height = defaultHeight;
                                packageList.Add(package);

                                root = new PackageNode();
                                root.box = new Box(-1, new Rect(0, 0, package.Width, package.Height));

                                contin = false;
                                startBox = i;
                                continue;
                            }

                            if (increaseSide)
                                package.Width <<= 1;
                            else
                                package.Height <<= 1;
                            increaseSide = !increaseSide;

                            if (package.Width == 0)
                                break;

                            // resize current package
                            contin = true;
                            root = new PackageNode();
                            root.box = new Box(-1, new Rect(0, 0, package.Width, package.Height));
                            break;
                        }

                        return null;
                    }
                }
                if (contin)
                    continue;

                break;
            }

            return packageList;
        }
    }
}
