﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HatchAnimEditor {
    public partial class SpritesheetList : Form {
        public SpritesheetList() {
            InitializeComponent();
        }

        public string currentFilename = "";
        public string spritesFolder = "";
        public List<int> newSheetPos = new List<int>();
        public bool modified = false;
        public bool reorder = false;

        private void listBoxHitboxes_DrawItem(object sender, DrawItemEventArgs e) {
            bool isSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            if (e.Index > -1) {
                Color color = isSelected ? SystemColors.Highlight : listBoxSpritesheetFilenames.BackColor;

                // Background item brush
                SolidBrush backgroundBrush = new SolidBrush(color);
                SolidBrush textBrush = new SolidBrush(e.ForeColor);

                RectangleF rectF = new RectangleF(e.Bounds.X + 2, e.Bounds.Y + 2, e.Bounds.Width, e.Bounds.Height);

                // Draw the background
                e.Graphics.FillRectangle(backgroundBrush, e.Bounds);
                e.Graphics.DrawString(listBoxSpritesheetFilenames.GetItemText(listBoxSpritesheetFilenames.Items[e.Index]), e.Font, textBrush, rectF, StringFormat.GenericDefault);

                // Clean up
                backgroundBrush.Dispose();
                textBrush.Dispose();
            }
            e.DrawFocusRectangle();
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
        }

        public static string prefix = "Sprites" + Path.DirectorySeparatorChar;

        private string GetSpritesFolder() {
            if (currentFilename != "") {
                spritesFolder = PathHelper.GetSpritesFolder(currentFilename);

                if (!PathHelper.EndsWith(spritesFolder, "Sprites")) {
                    Console.WriteLine("Animation file is not under a \"Sprites/\" directory!");
                    currentFilename = "";
                    spritesFolder = "";
                }
            }

            if (spritesFolder != "") {
                char lastChar = spritesFolder[spritesFolder.Length - 1];
                if (lastChar != '\\' && lastChar != Path.DirectorySeparatorChar)
                    spritesFolder += Path.DirectorySeparatorChar;
            }

            return spritesFolder;
        }

        private string StripSpritesFolderPath(string input, string spritesFolder) {
            string output = input;

            if (spritesFolder != "") {
                int index = input.IndexOf(spritesFolder);
                if (index >= 0)
                    output = input.Remove(index, spritesFolder.Length);
            }

            if (output.Contains(prefix))
                output = output.Substring(output.LastIndexOf(prefix) + prefix.Length).Replace(Path.DirectorySeparatorChar, '/');

            if (output[0] == Path.DirectorySeparatorChar)
                output = output.Remove(0, 1);

            return output;
        }

        private void listBoxHitboxes_DoubleClick(object sender, EventArgs e) {
            if (listBoxSpritesheetFilenames.Items.Count > 0 && listBoxSpritesheetFilenames.SelectedIndex >= 0) {
                string spritesFolder = GetSpritesFolder();

                OpenFileDialog ofd = new OpenFileDialog();
                if (currentFilename == "")
                    ofd.InitialDirectory = Directory.GetCurrentDirectory();
                else
                    ofd.InitialDirectory = spritesFolder;
                ofd.AddExtension = true;
                ofd.Filter = "GIF Files (*.gif)|*.gif|PNG Files (*.png)|*.png";

                if (ofd.ShowDialog() == DialogResult.OK) {
                    string sheetPath = StripSpritesFolderPath(ofd.FileName, spritesFolder);
                    listBoxSpritesheetFilenames.Items[listBoxSpritesheetFilenames.SelectedIndex] = sheetPath;
                    currentFilename = ofd.FileName;
                    modified = true;
                }
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e) {
            string spritesFolder = GetSpritesFolder();

            OpenFileDialog ofd = new OpenFileDialog();
            if (currentFilename == "")
                ofd.InitialDirectory = Directory.GetCurrentDirectory();
            else
                ofd.InitialDirectory = spritesFolder;
            ofd.AddExtension = true;
            ofd.Multiselect = true;
            ofd.Filter = "All supported formats (*.gif, *.png)|*.png;*.gif|GIF Files (*.gif)|*.gif|PNG Files (*.png)|*.png|All files (*.*)|*.*";

            if (ofd.ShowDialog() == DialogResult.OK) {
                foreach (String itFileName in ofd.FileNames)
                    currentFilename = itFileName;

                if (spritesFolder == "")
                    spritesFolder = GetSpritesFolder();

                Console.WriteLine("Sprites folder: " + spritesFolder);

                foreach (String itFileName in ofd.FileNames) {
                    string fileName = StripSpritesFolderPath(itFileName, spritesFolder);

                    Console.WriteLine("New spritesheet: " + fileName);

                    listBoxSpritesheetFilenames.Items.Add(fileName);

                    newSheetPos.Add(0); // Doesn't matter, because it's a new sheet.
                }

                modified = true;
            }
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e) {
            for (int i = listBoxSpritesheetFilenames.Items.Count - 1; i >= 0; i--) {
                if (listBoxSpritesheetFilenames.SelectedIndices.Contains(i)) {
                    listBoxSpritesheetFilenames.Items.RemoveAt(i);

                    newSheetPos[i] = -1; // Considered deleted (frames will use the first sheet)

                    for (int j = i; j < newSheetPos.Count; j++)
                        newSheetPos[j]--;

                    modified = true;
                    reorder = true;
                }
            }
        }
    }
}
