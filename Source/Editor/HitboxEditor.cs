﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HatchAnimEditor {
    public partial class HitboxEditor : Form {
        public HitboxEditor() {
            InitializeComponent();
        }

        public List<int> NewHitboxPos = new List<int>();
        public bool Modified = false;
        public bool Reorder = false;
        
        private void listBoxHitboxes_DrawItem(object sender, DrawItemEventArgs e) {
            bool isSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            if (e.Index > -1) {
                Color color = isSelected ? SystemColors.Highlight : listBoxHitboxes.BackColor;

                // Background item brush
                SolidBrush backgroundBrush = new SolidBrush(color);
                SolidBrush textBrush = new SolidBrush(e.ForeColor);

                RectangleF rectF = new RectangleF(e.Bounds.X + 2, e.Bounds.Y + 2, e.Bounds.Width, e.Bounds.Height);

                // Draw the background
                e.Graphics.FillRectangle(backgroundBrush, e.Bounds);
                e.Graphics.DrawString(listBoxHitboxes.GetItemText(listBoxHitboxes.Items[e.Index]), e.Font, textBrush, rectF, StringFormat.GenericDefault);

                // Clean up
                backgroundBrush.Dispose();
                textBrush.Dispose();
            }
            e.DrawFocusRectangle();
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
        }

        private void listBoxHitboxes_DoubleClick(object sender, EventArgs e) {
            if (listBoxHitboxes.Items.Count <= 0) return;
            if (listBoxHitboxes.SelectedIndex < 0) return; // Is this even possible?

            PromptDialog prompt = new PromptDialog("New name for \"" + listBoxHitboxes.SelectedItem + "\"?", listBoxHitboxes.SelectedItem.ToString());
            if (prompt.ShowDialog() == DialogResult.OK) {
                listBoxHitboxes.Items[listBoxHitboxes.SelectedIndex] = prompt.TextEntry.Text;
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e) {
            listBoxHitboxes.Items.Add("New Hitbox");

            Modified = true;
            NewHitboxPos.Add(0); // Doesn't matter, because it's a new hitbox.
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e) {
            for (int i = listBoxHitboxes.Items.Count - 1; i >= 0; i--) {
                if (listBoxHitboxes.SelectedIndices.Contains(i)) {
                    listBoxHitboxes.Items.RemoveAt(i);

                    NewHitboxPos[i] = -1; // Considered deleted
                    Modified = true;
                    Reorder = true;

                    for (int j = i + 1; j < NewHitboxPos.Count; j++)
                        NewHitboxPos[j]--;
                }
            }
        }
    }
}
