﻿using RSDKv5;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace HatchAnimEditor {
    public partial class AnimationEditor : Form {
        Point previousMousePos;

        public AnimationEditor() {
            InitializeComponent();

            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(this.AnimationEditor_DragEnter);
            this.DragDrop += new DragEventHandler(this.AnimationEditor_DragDrop);

            previousMousePos = MousePosition;

            splitContainer2.SplitterWidth = 1;
            splitContainerMain.SplitterWidth = 1;

            New();

            return;
        }

        class ASEConversionInfo {
            public int LoopFrameLayerIndex;
            public List<int> HitboxLayers;
            public List<int> DrawLayers;

            public List<int> frameMap;
            public List<ASE> frameASE;
            public List<int> frameDuration;
            public bool[] frameIsLoopFrame;
            public List<SpritePacker.Rect> frameCrops;
            public List<SpritePacker.Rect> frameOffsets;
            public List<SpritePacker.Rect[]> frameHitboxes;
            public List<System.Drawing.Color[]> frameBitmaps;
        }
        public void ASEtoRSDK_Convert(string[] asefiles, string binfilename) {
            // Read ASE files
            ASE[] aseList = new ASE[asefiles.Length];
            for (var i = 0; i < asefiles.Length; i++) {
                using (FileStream stream = new FileStream(asefiles[i], FileMode.Open))
                    aseList[i] = new ASE(stream);
            }

            // Gather ASE info
            ASEConversionInfo[] aseConversionInfos = new ASEConversionInfo[aseList.Length];
            for (var i = 0; i < aseList.Length; i++) {
                ASE ase = aseList[i];
                ASEConversionInfo convert = new ASEConversionInfo();
                convert.LoopFrameLayerIndex = -1;
                convert.HitboxLayers = new List<int>();
                convert.DrawLayers = new List<int>();
                
                ASELayerSelector prompt = new ASELayerSelector();
                for (var l = 0; l < ase.Layers.Count; l++) {
                    ASE.Layer layer = ase.Layers[l];
                    if (layer.Name.StartsWith("Hitbox:")) {
                        layer.Flags = 0;
                        convert.HitboxLayers.Add(l);
                        continue;
                    }
                    if (layer.Name.StartsWith("Loop Frame")) {
                        layer.Flags = 0;
                        convert.LoopFrameLayerIndex = l;
                        continue;
                    }
                    prompt.checkedListBoxLayers.Items.Add(layer.Name, (layer.Flags & (int)ASE.Layer.FlagTypes.Visible) != 0);
                    convert.DrawLayers.Add(l);
                }

                if (convert.DrawLayers.Count == 1 || prompt.ShowDialog() == DialogResult.OK) {
                    for (int l = 0; l < prompt.checkedListBoxLayers.Items.Count; l++) {
                        if (prompt.checkedListBoxLayers.GetItemChecked(l)) {
                            ase.Layers[convert.DrawLayers[l]].Flags |= (int)ASE.Layer.FlagTypes.Visible;
                        }
                        else {
                            ase.Layers[convert.DrawLayers[l]].Flags &= ~(int)ASE.Layer.FlagTypes.Visible;
                        }
                    }
                }
                else {
                    return;
                }

                aseConversionInfos[i] = convert;
            }

            /// Create spritesheets
            List<SpritePacker.Box> boxes = new List<SpritePacker.Box>();
            Dictionary<uint, int> hashToFrameIndex = new Dictionary<uint, int>();

            RSDKv5.Animation[] spriteFiles = new RSDKv5.Animation[asefiles.Length];
            for (var i = 0; i < aseList.Length; i++) {
                RSDKv5.Animation sprite = new RSDKv5.Animation();
                spriteFiles[i] = sprite;

                ASE ase = aseList[i];
                ASEConversionInfo convert = aseConversionInfos[i];

                convert.frameMap = new List<int>();
                convert.frameDuration = new List<int>();
                convert.frameASE = new List<ASE>();
                convert.frameBitmaps = new List<System.Drawing.Color[]>();
                convert.frameCrops = new List<SpritePacker.Rect>();
                convert.frameOffsets = new List<SpritePacker.Rect>();
                convert.frameHitboxes = new List<SpritePacker.Rect[]>();
                convert.frameIsLoopFrame = new bool[ase.Frames.Count];

                int offsetX = ase.Width / 2;
                int offsetY = ase.Height / 2;

                Console.WriteLine("" + asefiles[i]);
                Console.WriteLine("ase.Frames.Count: " + ase.Frames.Count);

                // Obtain all unique frame bitmaps
                for (int f = 0; f < ase.Frames.Count; f++) {
                    SpritePacker.Rect crop = new SpritePacker.Rect(ase.Width, ase.Height, 0, 0);

                    int canvasSize = ase.Width * ase.Height;

                    System.Drawing.Color[] frameCanvas = new System.Drawing.Color[canvasSize];
                    for (int p = 0; p < canvasSize; p++) {
                        frameCanvas[p] = System.Drawing.Color.Transparent;
                    }

                    // Get loop index if possible
                    if (convert.LoopFrameLayerIndex != -1) {
                        if (ase.ColorDepth == 8) {
                            for (int p = 0; p < canvasSize; p++) {
                                uint index = ase.Frames[f].PixelData[convert.LoopFrameLayerIndex][p];
                                if (index == ase.PaletteEntry)
                                    continue;

                                uint argb = ase.Palette[index];

                                uint color = (argb & 0xFF000000);
                                if (color != 0) {
                                    convert.frameIsLoopFrame[f] = true;
                                    break;
                                }
                            }
                        }
                        else if (ase.ColorDepth == 32) {
                            for (int p = 0; p < canvasSize; p++) {
                                uint argb = ase.Frames[f].PixelData[convert.LoopFrameLayerIndex][p];

                                uint color = (argb & 0xFF000000);
                                if (color != 0) {
                                    convert.frameIsLoopFrame[f] = true;
                                    break;
                                }
                            }
                        }
                    }

                    // Write pixels onto frame's canvas
                    if (convert.HitboxLayers.Count > 0)
                        convert.frameHitboxes.Add(new SpritePacker.Rect[convert.HitboxLayers.Count]);
                    else
                        convert.frameHitboxes.Add(null);

                    if (ase.ColorDepth == 8) {
                        for (int l = 0; l < ase.Frames[f].PixelData.Count; l++) {
                            bool isHitboxLayer = convert.HitboxLayers.Contains(l);

                            if (((ase.Layers[l].Flags & (int)ASE.Layer.FlagTypes.Visible) != 0 &&
                                (ase.Layers[l].Flags & (int)ASE.Layer.FlagTypes.Background) == 0) ||
                                isHitboxLayer) {
                                for (int p = 0; p < canvasSize; p++) {
                                    uint index = ase.Frames[f].PixelData[l][p];
                                    if (index == ase.PaletteEntry)
                                        continue;
                                    
                                    uint argb = ase.Palette[index];

                                    Console.WriteLine("index: " + index.ToString("X02") + " argb: " + argb.ToString("X08"));

                                    uint color = (argb & 0xFF000000);
                                    if (true) {
                                        int px = p % ase.Width;
                                        int py = p / ase.Width;
                                        if (isHitboxLayer) {
                                            int hitboxIndex = convert.HitboxLayers.IndexOf(l);
                                            if (convert.frameHitboxes.Last()[hitboxIndex] == null)
                                                convert.frameHitboxes.Last()[hitboxIndex] = new SpritePacker.Rect(ase.Width, ase.Height, 0, 0);
                                            SpritePacker.Rect hitbox = convert.frameHitboxes.Last()[hitboxIndex];
                                            hitbox.X = Math.Min(hitbox.X, px);
                                            hitbox.Y = Math.Min(hitbox.Y, py);
                                            hitbox.Width = Math.Max(hitbox.Width, px);
                                            hitbox.Height = Math.Max(hitbox.Height, py);
                                        }
                                        else {
                                            color |= (argb & 0xFF0000) >> 16;
                                            color |= (argb & 0xFF00);
                                            color |= (argb & 0xFF) << 16;
                                            frameCanvas[p] = System.Drawing.Color.FromArgb((int)index, 0, 0);

                                            crop.X = Math.Min(crop.X, px);
                                            crop.Y = Math.Min(crop.Y, py);
                                            crop.Width = Math.Max(crop.Width, px);
                                            crop.Height = Math.Max(crop.Height, py);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (ase.ColorDepth == 32) {
                        for (int l = 0; l < ase.Frames[f].PixelData.Count; l++) {
                            bool isHitboxLayer = convert.HitboxLayers.Contains(l);

                            if (((ase.Layers[l].Flags & (int)ASE.Layer.FlagTypes.Visible) != 0 &&
                                (ase.Layers[l].Flags & (int)ASE.Layer.FlagTypes.Background) == 0) ||
                                isHitboxLayer) {
                                for (int p = 0; p < canvasSize; p++) {
                                    uint argb = ase.Frames[f].PixelData[l][p];
                                    
                                    uint color = (argb & 0xFF000000);
                                    if (color != 0) {
                                        int px = p % ase.Width;
                                        int py = p / ase.Width;
                                        if (isHitboxLayer) {
                                            int hitboxIndex = convert.HitboxLayers.IndexOf(l);
                                            if (convert.frameHitboxes.Last()[hitboxIndex] == null)
                                                convert.frameHitboxes.Last()[hitboxIndex] = new SpritePacker.Rect(ase.Width, ase.Height, 0, 0);
                                            SpritePacker.Rect hitbox = convert.frameHitboxes.Last()[hitboxIndex];
                                            hitbox.X = Math.Min(hitbox.X, px);
                                            hitbox.Y = Math.Min(hitbox.Y, py);
                                            hitbox.Width = Math.Max(hitbox.Width, px);
                                            hitbox.Height = Math.Max(hitbox.Height, py);
                                        }
                                        else {
                                            color |= (argb & 0xFF0000) >> 16;
                                            color |= (argb & 0xFF00);
                                            color |= (argb & 0xFF) << 16;
                                            frameCanvas[p] = System.Drawing.Color.FromArgb((int)color);

                                            crop.X = Math.Min(crop.X, px);
                                            crop.Y = Math.Min(crop.Y, py);
                                            crop.Width = Math.Max(crop.Width, px);
                                            crop.Height = Math.Max(crop.Height, py);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Adjust Hitboxes
                    for (var h = 0; h < convert.HitboxLayers.Count; h++) {
                        SpritePacker.Rect hitbox = convert.frameHitboxes.Last()[h];
                        if (hitbox == null)
                            hitbox = new SpritePacker.Rect(0, 0, 0, 0);
                        hitbox.X -= offsetX;
                        hitbox.Y -= offsetY;
                        hitbox.Width -= offsetX;
                        hitbox.Height -= offsetY;
                        hitbox.Width += 1;
                        hitbox.Height += 1;
                        // hitbox.Width -= hitbox.X - 1;
                        // hitbox.Height -= hitbox.Y - 1;
                        // hitbox.Width = Math.Max(hitbox.Width, 0);
                        // hitbox.Height = Math.Max(hitbox.Height, 0);
                        convert.frameHitboxes.Last()[h] = hitbox;
                    }

                    // Hash canvas to check for uniqueness
                    uint frameHash = 0xDEADBEEF;
                    for (int p = 0; p < canvasSize; p++) {
                        int px = p % ase.Width;
                        int py = p / ase.Width;
                        // Width and Height in this respect are just X2 and Y2
                        if (px >= crop.X && py >= crop.Y &&
                            px <= crop.Width && py <= crop.Height) {
                            frameHash = Jenkins.HashUInt32((uint)frameCanvas[p].ToArgb(), frameHash);
                        }
                    }

                    // Adjust Box from X1Y1X2Y2 to XYWH
                    crop.Width -= crop.X - 1;
                    crop.Height -= crop.Y - 1;
                    crop.Width = Math.Max(crop.Width, 0);
                    crop.Height = Math.Max(crop.Height, 0);

                    // Add frame properties
                    convert.frameASE.Add(ase);
                    convert.frameCrops.Add(crop);
                    convert.frameBitmaps.Add(frameCanvas);
                    if (ase.Frames[f].FrameDuration == 1)
                        convert.frameDuration.Add(0);
                    else
                        convert.frameDuration.Add((ase.Frames[f].FrameDuration * 60 + 999) / 1000); // ceil

                    if (hashToFrameIndex.ContainsKey(frameHash)) {
                        convert.frameMap.Add(hashToFrameIndex[frameHash]);
                        convert.frameOffsets.Add(new SpritePacker.Rect(crop.X - offsetX, crop.Y - offsetY, 0, 0));

                        Console.WriteLine("ID: " + (f + 1) + 
                            "  frameHash: " + frameHash.ToString("X08") +
                            "  crop.X: " + crop.X + " crop.Y: " + crop.Y +
                            "  crop.Width: " + crop.Width + " crop.Height: " + crop.Height +
                            "  offsetX: " + offsetX + " offsetY: " + offsetY);
                    }
                    else {
                        hashToFrameIndex.Add(frameHash, boxes.Count);
                        convert.frameMap.Add(boxes.Count);
                        convert.frameOffsets.Add(new SpritePacker.Rect(crop.X - offsetX, crop.Y - offsetY, 0, 0));

                        SpritePacker.Box box = new SpritePacker.Box(boxes.Count, new SpritePacker.Rect(0, 0, crop.Width, crop.Height));
                        box.OffX = crop.X - offsetX;
                        box.OffY = crop.Y - offsetY;
                        Console.WriteLine("ID: " + (f + 1) +
                            "  frameHash: " + frameHash.ToString("X08") +
                            "  X: " + box.OffX + " Y: " + box.OffY +
                            "  crop.X: " + crop.X + " crop.Y: " + crop.Y +
                            "  crop.Width: " + crop.Width + " crop.Height: " + crop.Height +
                            "  offsetX: " + offsetX + " offsetY: " + offsetY);
                        boxes.Add(box);
                    }
                }
            }

            // Package all unique frames
            int maxSheetWidth = 16386;
            int maxSheetHeight = 16386;
            List<SpritePacker.Package> packages = SpritePacker.PackBoxes(ref boxes, true, 8, 8, maxSheetWidth, maxSheetHeight);

            // Check ColorDepth
            bool all8bpp = true;
            for (var i = 0; i < aseList.Length; i++) {
                ASE ase = aseList[i];
                all8bpp &= ase.ColorDepth == 8;
                if (!all8bpp)
                    break;
            }
            
            // Log useful data
            Console.WriteLine("Unique Frame Count: " + hashToFrameIndex.Count);
            Console.WriteLine("Unique Boxes Count: " + boxes.Count);

            // Create bitmaps from packages
            List<Bitmap> bitmapAtlases = new List<Bitmap>();
            List<byte[]> bitmapAtlasBytes = new List<byte[]>();
            List<BitmapData> bitmapAtlasDatas = new List<BitmapData>();
            foreach (SpritePacker.Package package in packages) {
                Bitmap bitmap;
                ASE ase = aseList[0];
                if (all8bpp) {
                    bitmap = new Bitmap(package.Width, package.Height, PixelFormat.Format8bppIndexed);

                    ColorPalette cpal = bitmap.Palette;
                    for (int i = 0; i < 256; i++) {
                        if (i < ase.Palette.Length) {
                            int color = 0;
                            int argb = (int)ase.Palette[i];
                            color |= (argb & 0xFF0000) >> 16;
                            color |= (argb & 0xFF00);
                            color |= (argb & 0xFF) << 16;
                            cpal.Entries[i] = System.Drawing.Color.FromArgb(color);
                        }
                        else
                            cpal.Entries[i] = System.Drawing.Color.FromArgb(0xFF00FF);
                    }

                    bitmap.Palette = cpal;
                    bitmapAtlasDatas.Add(bitmap.LockBits(new Rectangle(0, 0, package.Width, package.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat));

                    byte[] bytes = new byte[package.Width * package.Height];
                    Marshal.Copy(bitmapAtlasDatas.Last().Scan0, bytes, 0, bytes.Length);
                    bitmapAtlasBytes.Add(bytes);
                }
                else {
                    bitmap = new Bitmap(package.Width, package.Height, PixelFormat.Format32bppArgb);
                }
                bitmapAtlases.Add(bitmap);
            }

            // Add unique frames to bitmaps
            int idStart = 0;
            Dictionary<int, int> AddedBoxID = new Dictionary<int, int>();
            for (var a = 0; a < aseList.Length; a++) {
                ASE ase = aseList[a];
                ASEConversionInfo convert = aseConversionInfos[a];

                for (var i = 0; i < convert.frameMap.Count; i++) {
                    int uniqueFrameIndex = convert.frameMap[i];
                    SpritePacker.Box boxx = boxes[uniqueFrameIndex];
                    SpritePacker.Rect crop = convert.frameCrops[i];
                    int canvasSize = ase.Width * ase.Height;

                    if (!AddedBoxID.ContainsKey(uniqueFrameIndex)) {
                        Console.WriteLine("box id: " + uniqueFrameIndex + "    x: " + boxx.Rect.X + " y: " + boxx.Rect.Y + " sheet: " + boxx.PackageID);
                        for (int p = 0; p < canvasSize; p++) {
                            System.Drawing.Color c = convert.frameBitmaps[i][p];

                            int px = p % ase.Width;
                            int py = p / ase.Width;
                            if (c.A > 0) {
                                px -= crop.X;
                                py -= crop.Y;

                                px += boxx.Rect.X;
                                py += boxx.Rect.Y;

                                if (all8bpp) {
                                    BitmapData data = bitmapAtlasDatas[boxx.PackageID];
                                    byte[] bytes = bitmapAtlasBytes[boxx.PackageID];
                                    bytes[px + py * data.Stride] = c.R;
                                }
                                else {
                                    bitmapAtlases[boxx.PackageID].SetPixel(px, py, c);
                                }
                            }
                        }
                        AddedBoxID.Add(uniqueFrameIndex, 1);
                    }
                }

                idStart += convert.frameMap.Count;
            }

            // Unlock bitmaps
            for (var i = 0; i < bitmapAtlasDatas.Count; i++) {
                Marshal.Copy(bitmapAtlasBytes[i], 0, bitmapAtlasDatas[i].Scan0, bitmapAtlasBytes[i].Length);
                bitmapAtlases[i].UnlockBits(bitmapAtlasDatas[i]);
            }

            // Save bitmaps
            bool singleAnim = true;
            if (aseList.Length != 1)
                singleAnim = false;

            string outFilename = Path.GetFileNameWithoutExtension(binfilename);

            string outFilenamePath = binfilename;
            string outTileFolder = Directory.GetParent(binfilename).FullName;
            if (!singleAnim)
                outFilenamePath = outTileFolder;

            bool isUnderSpritesDirectory = true;

            string sheetRelFilename = binfilename;
            string spritesFolder = PathHelper.GetSpritesFolder(outFilenamePath);

            if (!PathHelper.EndsWith(spritesFolder, "Sprites")) {
                Console.WriteLine("Animation file is not under a \"Sprites/\" directory!");
                isUnderSpritesDirectory = false;
            }
            else {
                sheetRelFilename = Directory.GetParent(sheetRelFilename).FullName;
                if (spritesFolder.Length < sheetRelFilename.Length) {
                    sheetRelFilename = sheetRelFilename.Substring(spritesFolder.Length + 1);
                }
            }

            int atlasIndex = 0;
            string fileExtension = ".gif";
            if (!all8bpp)
                fileExtension = ".png";

            List<string> spritesheetNames = new List<string>();

            foreach (Bitmap atlas in bitmapAtlases) {
                string suffix;
                if (bitmapAtlases.Count != 1)
                    suffix = (atlasIndex + fileExtension);
                else
                    suffix = fileExtension;

                string sheetFilename;
                if (!isUnderSpritesDirectory)
                    sheetFilename = "./" + outFilename;
                else
                    sheetFilename = sheetRelFilename + "/" + outFilename;

                sheetFilename = sheetFilename.Replace('\\', '/') + suffix;
                spritesheetNames.Add(sheetFilename);

                string outSheetFilename = Path.Combine(outTileFolder, outFilename + suffix);
                if (fileExtension == ".gif")
                    atlas.Save(outSheetFilename, ImageFormat.Gif);
                else
                    atlas.Save(outSheetFilename);

                atlasIndex++;
            }

            /// Create Animation Files
            for (var a = 0; a < aseList.Length; a++) {
                RSDKv5.Animation sprite = spriteFiles[a];

                ASE ase = aseList[a];
                ASEConversionInfo convert = aseConversionInfos[a];

                // Add hitboxes
                for (var l = 0; l < convert.HitboxLayers.Count; l++) {
                    sprite.CollisionBoxes.Add(ase.Layers[convert.HitboxLayers[l]].Name.Substring("Hitbox:".Length).TrimStart());
                }
                // Add spritesheets
                foreach (string sheet in spritesheetNames) {
                    sprite.SpriteSheets.Add(sheet);
                }

                // Add animation entries
                for (int i = 0; i < ase.AnimRanges.Count; i++) {
                    var range = ase.AnimRanges[i];
                    var animEntry = new RSDKv5.Animation.AnimationEntry(range.Name);
                    animEntry.FrameSpeed = 1;

                    Console.WriteLine("range.Name: " + range.Name);

                    for (int f = range.Start; f <= range.End; f++) {
                        int uniqueFrameIndex = convert.frameMap[f];
                        SpritePacker.Box boxx = boxes[uniqueFrameIndex];

                        RSDKv5.Animation.Frame fr = new RSDKv5.Animation.Frame(boxx.Rect.Width, boxx.Rect.Height);
                        fr.CenterX = convert.frameOffsets[f].X; // boxx.OffX;
                        fr.CenterY = convert.frameOffsets[f].Y; //  boxx.OffY;
                        fr.Duration = convert.frameDuration[f];
                        fr.X = boxx.Rect.X;
                        fr.Y = boxx.Rect.Y;
                        fr.SpriteSheet = boxx.PackageID;
                        fr.ID = f + 1;

                        Console.WriteLine("ase frame: " + (f + 1) + " -> box id: " + uniqueFrameIndex + "    x: " + boxx.Rect.X + " y: " + boxx.Rect.Y + " sheet: " + boxx.PackageID + "    fr.X: " + fr.X + " fr.Y: " + fr.Y);

                        if (convert.frameIsLoopFrame[f] && animEntry.FrameLoop == 0)
                            animEntry.FrameLoop = f - range.Start;

                        if (convert.frameHitboxes[f] != null) {
                            for (var h = 0; h < convert.frameHitboxes[f].Length; h++) {
                                RSDKv5.Animation.Frame.HitBox hitBox = new RSDKv5.Animation.Frame.HitBox();
                                hitBox.Left = convert.frameHitboxes[f][h].X;
                                hitBox.Top = convert.frameHitboxes[f][h].Y;
                                hitBox.Right = convert.frameHitboxes[f][h].Width;
                                hitBox.Bottom = convert.frameHitboxes[f][h].Height;
                                fr.HitBoxes.Add(hitBox);
                            }
                        }

                        animEntry.Frames.Add(fr);
                    }
                    sprite.Animations.Add(animEntry);
                }

                // Save animation file
                string fileName;

                if (!singleAnim)
                    fileName = Path.Combine(outFilenamePath, Path.GetFileNameWithoutExtension(asefiles[a])) + ".bin";
                else
                    fileName = outFilenamePath;

                Console.WriteLine("Output sprite: " + fileName);

                using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate)) {
                    using (Writer writer = new Writer(fs))
                        sprite.Save(writer);
                }
            }
        }

        public List<Bitmap> spriteSheets = new List<Bitmap>();

        RSDKv5.Animation currentAnimation = null;
        int currentAnimationIndex = -1;
        int currentFrame = 0;
        string currentFilename = "";
        string currentWindowTitle = "";

        int zoom = 1;
        bool showBounds = false;
        bool showHitbox = false;
        bool showEntryID = false;

        bool noUpdate = false;
        bool preventChange = false;
        bool fileModified = false;

        ResizableHitbox resizableHitbox = null;

        string GetProgramName() {
            return "Hatch Animation Editor";
        }

        void SetWindowTitle(string fileName) {
            currentWindowTitle = GetProgramName() + " - " + fileName;
            UpdateWindowTitle();
        }

        void UpdateWindowTitle() {
            this.Text = currentWindowTitle;

            if (fileModified)
                this.Text += " (*)";
        }

        void New() {
            currentAnimation = new RSDKv5.Animation();
            fileModified = false;

            this.currentFilename = "";

            SetWindowTitle(GetDefaultFileName());
            ResetUI();
        }

        void SetFileModified() {
            fileModified = true;
            UpdateWindowTitle();
        }

        // Asks if the user wants to store unsaved changes
        bool AskSaveModified() {
            if (!fileModified)
                return true;

            DialogResult choice = MessageBox.Show("Do you want to save the changes you made to " + GetFileName() + "?",
                "Unsaved changes", MessageBoxButtons.YesNoCancel);

            switch (choice) {
                case DialogResult.Yes:
                    Save(false);
                    break;
                case DialogResult.No:
                    break;
                case DialogResult.Cancel:
                    return false;
            }

            return true;
        }

        string GetDefaultFileName() {
            return "New Sprite.bin";
        }

        string GetFileName() {
            if (this.currentFilename == "")
                return GetDefaultFileName();

            string[] spp = this.currentFilename.Split(Path.DirectorySeparatorChar);
            return spp[spp.Length - 1];
        }

        void Open(string filename) {
            using (FileStream input = File.OpenRead(filename)) {
                using (Reader reader = new Reader(input)) {
                    currentAnimation = new RSDKv5.Animation();
                    currentAnimation.Load(reader);
                    fileModified = false;

                    this.currentFilename = filename;
                    SetWindowTitle(GetFileName());

                    ResetUI();

                    // Select the first animation
                    if (currentAnimation.Animations.Count > 0)
                        listBoxAnimations.SetSelected(0, true);
                }
            }
        }

        void SaveAs(string filename) {
            using (FileStream output = File.OpenWrite(filename)) {
                using (Writer writer = new Writer(output)) {
                    currentAnimation.Save(writer);
                    currentFilename = filename;
                    fileModified = false;

                    this.currentFilename = filename;
                    SetWindowTitle(GetFileName());
                }
            }
            UpdateMainPreviewUI();
        }

        void Save(bool showDialog) {
            string fn = this.currentFilename;
            if (fn == "" || showDialog) {
                SaveFileDialog svd = new SaveFileDialog();
                svd.RestoreDirectory = true;
                svd.AddExtension = true;
                svd.Filter = "BIN Files (*.bin)|*.bin|All files (*.*)|*.*";
                if (svd.ShowDialog() == DialogResult.OK) {
                    fn = svd.FileName;
                }
            }
            if (fn != "") {
                SaveAs(fn);
            }
        }

        bool IsAnimIndexValid() {
            if (currentAnimation.Animations.Count == 0)
                return false;
            return currentAnimationIndex >= 0;
        }
        bool IsFrameIndexValid() {
            if (!IsAnimIndexValid())
                return false;
            RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
            if (anim.Frames.Count == 0)
                return false;
            return currentFrame >= 0 && currentFrame < anim.Frames.Count;
        }

        RSDKv5.Animation.AnimationEntry GetCurrentAnimation() {
            if (IsAnimIndexValid())
                return currentAnimation.Animations[currentAnimationIndex];
            return null;
        }
        RSDKv5.Animation.Frame GetCurrentFrame() {
            RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
            if (anim != null && IsFrameIndexValid())
                return anim.Frames[currentFrame];
            return null;
        }
        RSDKv5.Animation.Frame.HitBox GetCurrentHitbox() {
            RSDKv5.Animation.Frame frame = GetCurrentFrame();
            if (frame == null)
                return null;

            int selectedHitbox = comboBoxHitbox.SelectedIndex;
            if (selectedHitbox < 0)
                return null;

            return frame.HitBoxes[selectedHitbox];
        }

        void ResetUI() {
            currentAnimationIndex = -1;
            currentFrame = 0;

            UpdateAnimationListUI();
            listBoxAnimations.ClearSelected();

            for (int i = 0; i < spriteSheets.Count; i++) {
                spriteSheets[i].Dispose();
            }
            spriteSheets.Clear();

            string spritesFolder = "";
            if (currentFilename != "") {
                spritesFolder = PathHelper.GetSpritesFolder(currentFilename);

                if (!PathHelper.EndsWith(spritesFolder, "Sprites")) {
                    Console.WriteLine("Animation file is not under a \"Sprites/\" directory!");
                    spritesFolder = Directory.GetParent(currentFilename).FullName;
                }
            }

            int ogIndex = comboBoxTexture.SelectedIndex;

            UpdatePreviewSheets(spritesFolder);
            UpdateSheetList();

            if (ogIndex < comboBoxTexture.Items.Count)
                comboBoxTexture.SelectedIndex = ogIndex;
            else if (comboBoxTexture.Items.Count > 0)
                comboBoxTexture.SelectedIndex = 0;

            UpdateMainPreviewUI();
        }
        void UpdateMainPreviewUI() {
            UpdatePreviewUI();
            UpdateFrameButtonsDock();
        }
        void UpdateFrameButtonsDock() {
            toolStrip1.Enabled = currentAnimation.Animations.Count > 0 && IsAnimIndexValid();
        }
        // Convert the sheet to an ARGB bitmap
        Bitmap MakeSheetBitmap(string path) {
            Bitmap sheet = new Bitmap(path);
            Bitmap result = new Bitmap(sheet.Width, sheet.Height, PixelFormat.Format32bppArgb);
            using (var g = Graphics.FromImage(result))
                g.DrawImage(sheet, new Rectangle(0, 0, sheet.Width, sheet.Height));
            return result;
        }
        void UpdatePreviewSheets(string spritesFolder) {
            comboBoxTexture.Items.Clear();

            for (int i = 0; i < currentAnimation.SpriteSheets.Count; i++) {
                string sheet = currentAnimation.SpriteSheets[i].Replace("\\", "/");
                string fullSheetPath = spritesFolder;

                if (sheet.StartsWith("./")) {
                    sheet = sheet.Substring(2);
                    fullSheetPath = Directory.GetParent(currentFilename).FullName;
                }

                if (!PathHelper.HasSeparator(fullSheetPath))
                    fullSheetPath += Path.DirectorySeparatorChar;

                fullSheetPath += sheet;

                if (File.Exists(fullSheetPath))
                    spriteSheets.Add(MakeSheetBitmap(fullSheetPath));
                else if (File.Exists(sheet))
                    spriteSheets.Add(MakeSheetBitmap(sheet));
                else
                    spriteSheets.Add(new Bitmap(512, 512));
            }
        }
        void UpdateSheetList() {
            comboBoxTexture.Items.Clear();
            for (int i = 0; i < currentAnimation.SpriteSheets.Count; i++) {
                comboBoxTexture.Items.Add(currentAnimation.SpriteSheets[i].Replace("\\", "/"));
            }
        }
        void UpdatePreviewUI() {
            listViewFrames.Items.Clear();

            if (!IsAnimIndexValid()) {
                comboBoxTexture.Items.Clear();

                splitContainerFrame.Enabled = false;

                numericUpDownSpeed.Value = 0;
                numericUpDownLoopIndex.Value = 0;
                comboBoxFlags.SelectedIndex = 0;
            } else {
                splitContainerFrame.Enabled = true;

                RSDKv5.Animation.AnimationEntry animationEntry = GetCurrentAnimation();
                numericUpDownSpeed.Value = animationEntry.FrameSpeed;
                numericUpDownLoopIndex.Value = animationEntry.FrameLoop;
                comboBoxFlags.SelectedIndex = animationEntry.Unknown;

                for (int i = 0; i < animationEntry.Frames.Count; i++) {
                    listViewFrames.Items.Add(i.ToString());
                }
            }

            UpdateFrameUI();
            UpdateHitboxList();
        }
        void UpdateHitboxList() {
            int numBoxes = currentAnimation.CollisionBoxes.Count;
            int lastSelected = Math.Max(comboBoxHitbox.SelectedIndex, 0);

            comboBoxHitbox.Items.Clear();

            if (numBoxes > 0) {
                for (int i = 0; i < numBoxes; i++)
                    comboBoxHitbox.Items.Add(currentAnimation.CollisionBoxes[i]);
                comboBoxHitbox.SelectedIndex = Math.Min(numBoxes - 1, lastSelected);
            }
        }
        // Creates the bitmap for the frame preview
        unsafe Bitmap CreatePreviewBitmap(Bitmap frameBitmap, RSDKv5.Animation.Frame frame) {
            if (frame.SpriteSheet >= spriteSheets.Count || frame.Width < 1 || frame.Height < 1 || frame.X >= frameBitmap.Width || frame.Y >= frameBitmap.Height)
                return null;

            int frameWidth = frame.Width;
            int frameHeight = frame.Height;

            int scaledWidth = frameWidth * zoom;
            int scaledHeight = frameHeight * zoom;

            double xstep = (float)frameWidth / (float)scaledWidth;
            double ystep = (float)frameHeight / (float)scaledHeight;

            Bitmap resultBitmap = new Bitmap(scaledWidth, scaledHeight);

            // Crop the bitmap
            int right = frame.X + frameWidth;
            if (right > frameBitmap.Width)
                frameWidth -= right - frameBitmap.Width;

            int bitmapHeight = frameHeight;
            int bottom = frame.Y + frameHeight;
            if (bottom > frameBitmap.Height)
                frameHeight -= bottom - frameBitmap.Height;

            // Lock the bitmap data
            // We'll be writing into the buffers directly.
            BitmapData srcData = frameBitmap.LockBits(new Rectangle(frame.X, frame.Y, frameWidth, frameHeight), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData destData = resultBitmap.LockBits(new Rectangle(0, 0, scaledWidth, scaledHeight), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int srcStride = srcData.Stride;
            int destStride = destData.Stride;
            byte *srcPtr = (byte*)srcData.Scan0;
            byte *destPtr = (byte*)destData.Scan0;

            double ysrc = 0.0f;

            for (int y = 0; y < scaledHeight; y++) {
                double xsrc = 0.0f;

                int src_iy = (int)ysrc;
                if (src_iy >= frameHeight)
                    break;

                int srcOffsetY = src_iy * srcStride;
                int destOffset = y * destStride;

                for (int x = 0; x < scaledWidth; x++) {
                    int src_ix = (int)xsrc;
                    int srcOffset = srcOffsetY + (src_ix * 4);

                    byte r, g, b, a;
                    if (src_ix < frameWidth) {
                        a = srcPtr[srcOffset + 0];
                        r = srcPtr[srcOffset + 1];
                        g = srcPtr[srcOffset + 2];
                        b = srcPtr[srcOffset + 3];
                    }
                    else
                        r = g = b = a = 0;

                    destPtr[destOffset + 0] = a;
                    destPtr[destOffset + 1] = r;
                    destPtr[destOffset + 2] = g;
                    destPtr[destOffset + 3] = b;

                    destOffset += 4;
                    xsrc += xstep;
                }

                ysrc += ystep;
            }

            // Unlock the bitmaps when we're done
            frameBitmap.UnlockBits(srcData);
            resultBitmap.UnlockBits(destData);

            return resultBitmap;
        }
        void UpdateFrameUI() {
            RSDKv5.Animation.Frame frame = GetCurrentFrame();
            if (frame != null) {
                preventChange = true;
                numericUpDownCenterX.Value = frame.CenterX;
                numericUpDownCenterY.Value = frame.CenterY;
                numericUpDownLeft.Value = frame.X;
                numericUpDownTop.Value = frame.Y;
                numericUpDownWidth.Value = frame.Width;
                numericUpDownHeight.Value = frame.Height;
                numericUpDownID.Value = frame.ID;
                numericUpDownDuration.Value = frame.Duration;
                if (comboBoxTexture.Items.Count > 0)
                    comboBoxTexture.SelectedIndex = frame.SpriteSheet;
                preventChange = false;

                groupBoxFrame.Text = "Current Entry: " + currentAnimationIndex + " - Current Frame: " + currentFrame + " - Total: " + GetCurrentAnimation().Frames.Count;
                groupBoxFrame.Enabled = true;
            } else {
                preventChange = true;
                numericUpDownCenterX.Value = 0;
                numericUpDownCenterY.Value = 0;
                numericUpDownLeft.Value = 0;
                numericUpDownTop.Value = 0;
                numericUpDownWidth.Value = 0;
                numericUpDownHeight.Value = 0;
                numericUpDownID.Value = 0;
                numericUpDownDuration.Value = 0;
                if (comboBoxTexture.Items.Count > 0)
                    comboBoxTexture.SelectedIndex = 0;
                preventChange = false;

                groupBoxFrame.Text = "Current Entry: 0 - Current Frame: 0 - Total: 0";
                groupBoxFrame.Enabled = false;
            }

            comboBoxHitbox_SelectedIndexChanged(null, null);
            UpdateResizableHitbox();
            panelPreview.Refresh();
        }
        void UpdateResizableHitbox() {
            if (!showHitbox) {
                resizableHitbox = null;
                return;
            }

            RSDKv5.Animation.Frame.HitBox hitbox = GetCurrentHitbox();
            if (hitbox == null) {
                resizableHitbox = null;
                return;
            }

            Rectangle rect = new Rectangle(hitbox.Left, hitbox.Top, hitbox.Right - hitbox.Left, hitbox.Bottom - hitbox.Top);
            if (resizableHitbox == null)
                resizableHitbox = new ResizableHitbox(rect);
            else
                resizableHitbox.Update(rect);
        }
        void UpdateAnimationListUI() {
            if (listBoxAnimations.Items.Count != currentAnimation.Animations.Count) {
                listBoxAnimations.Items.Clear();
                if (showEntryID) {
                    for (int i = 0; i < currentAnimation.Animations.Count; i++)
                        listBoxAnimations.Items.Add(i + ": " + currentAnimation.Animations[i].AnimName);
                }
                else {
                    for (int i = 0; i < currentAnimation.Animations.Count; i++)
                        listBoxAnimations.Items.Add(currentAnimation.Animations[i].AnimName);
                }
                listBoxAnimations.ClearSelected();
            }
            else {
                if (showEntryID) {
                    for (int i = 0; i < currentAnimation.Animations.Count; i++) {
                        listBoxAnimations.Items[i] = i + ": " + currentAnimation.Animations[i].AnimName;
                    }
                }
                else {
                    for (int i = 0; i < currentAnimation.Animations.Count; i++) {
                        listBoxAnimations.Items[i] = currentAnimation.Animations[i].AnimName;
                    }
                }
            }
        }
        void SwitchAnimationListEntries(int e1, int e2) {
            var entry = listBoxAnimations.Items[e1];
            listBoxAnimations.Items.RemoveAt(e1);
            listBoxAnimations.Items.Insert(e2, entry);
        }

        void PutOnClipboard(Byte[] byteArr) {
            DataObject data = new DataObject();
            using (MemoryStream memStream = new MemoryStream()) {
                memStream.Write(byteArr, 0, byteArr.Length);
                data.SetData("rawbinary", false, memStream);
                Clipboard.SetDataObject(data, true);
            }
        }
        byte[] GetFromClipboard() {
            DataObject retrievedData = Clipboard.GetDataObject() as DataObject;
            if (retrievedData == null) {
                Console.WriteLine("GetFromClipboard: Clipboard is empty");
                return null;
            } else if (!retrievedData.GetDataPresent("rawbinary")) {
                Console.WriteLine("GetFromClipboard: Clipboard doesn't contain binary data");
                return null;
            }

            MemoryStream byteStream = retrievedData.GetData("rawbinary") as MemoryStream;
            if (byteStream == null) {
                Console.WriteLine("GetFromClipboard: Couldn't retrieve byte stream");
                return null;
            }

            return byteStream.ToArray();
        }

        private void paintRectNoFill(Graphics g, Rectangle rect, SolidBrush color) {
            g.FillRectangle(color, rect.Left, rect.Top, rect.Width, zoom);
            g.FillRectangle(color, rect.Left, rect.Top + rect.Height, rect.Width, zoom);
            g.FillRectangle(color, rect.Left, rect.Top, zoom, rect.Height);
            g.FillRectangle(color, rect.Left + rect.Width, rect.Top, zoom, rect.Height + zoom);
        }
        private void paintRectNoFillDashed(Graphics g, Rectangle rect, SolidBrush color) {
            Pen pen = new Pen(color, zoom);
            float[] pattern = { 4, 2 };
            pen.DashPattern = pattern;

            g.DrawLine(pen, new Point(rect.Left, rect.Top), new Point(rect.Right, rect.Top));
            g.DrawLine(pen, new Point(rect.Left, rect.Bottom), new Point(rect.Right, rect.Bottom));
            g.DrawLine(pen, new Point(rect.Left, rect.Top), new Point(rect.Left, rect.Bottom));
            g.DrawLine(pen, new Point(rect.Right, rect.Top), new Point(rect.Right, rect.Bottom));
        }

        private SolidBrush MakeColor(int red, int green, int blue, int alpha) {
            return new SolidBrush(System.Drawing.Color.FromArgb(alpha, red, green, blue));
        }
        private SolidBrush MakeColor(int red, int green, int blue) {
            return MakeColor(red, green, blue, 0xFF);
        }

        private void panelPreview_Paint(object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;

            if (currentAnimation.Animations.Count > 0 && spriteSheets.Count > 0 && IsAnimIndexValid()) {
                RSDKv5.Animation.AnimationEntry animationEntry = GetCurrentAnimation();

                int frameCount = animationEntry.Frames.Count;
                if (frameCount < 1 || currentFrame >= frameCount) {
                    listViewFrames.Refresh();
                    return;
                }

                RSDKv5.Animation.Frame frame = animationEntry.Frames[currentFrame];

                Bitmap bitmap = CreatePreviewBitmap(spriteSheets[frame.SpriteSheet], frame);

                int w = frame.Width;
                int h = frame.Height;
                int x = panelPreview.Width / 2;
                int y = panelPreview.Height / 2;

                g.DrawLine(Pens.Black, x, y - h, x, y + h);
                g.DrawLine(Pens.Black, x - w, y, x + w, y);

                if (bitmap != null) {
                    Rectangle destRect = new Rectangle(x + frame.CenterX * zoom, y + frame.CenterY * zoom, w * zoom, h * zoom);
                    Rectangle srcRect = new Rectangle(0, 0, w * zoom, h * zoom);
                    g.DrawImage(bitmap, destRect, srcRect, GraphicsUnit.Pixel);
                }

                if (showBounds) {
                    int left = x + (frame.CenterX - 1) * zoom;
                    int top = y + (frame.CenterY - 1) * zoom;
                    int width = (w + 1) * zoom;
                    int height = (h + 1) * zoom;
                    paintRectNoFill(g, new Rectangle(left, top, width, height), MakeColor(0xFF, 0x00, 0x00));
                }

                if (showHitbox && resizableHitbox != null && comboBoxHitbox.SelectedItem != null) {
                    Rectangle hitboxRect = resizableHitbox.GetPanelHitbox(x, y, zoom);
                    g.FillRectangle(MakeColor(0xFF, 0xFF, 0xFF, 0x80), hitboxRect);

                    SolidBrush color;
                    int highlighted = resizableHitbox.GetHighlightedHandle();
                    int dragged = resizableHitbox.GetDraggedHandle();
                    if (highlighted == (int)DragPoint.All) {
                        if (dragged == highlighted)
                            color = MakeColor(0x00, 0x00, 0x00);
                        else
                            color = MakeColor(0xFF, 0xFF, 0x00);
                        paintRectNoFillDashed(g, hitboxRect, color);
                    }

                    if (highlighted != -1 && dragged != (int)DragPoint.All) {
                        for (int i = 0; i < 8; i++) {
                            Rectangle rect = resizableHitbox.GetPanelHandle((DragPoint)i, x, y, zoom);
                            if (dragged == i)
                                color = MakeColor(0x00, 0x00, 0x00);
                            else if (highlighted == i)
                                color = MakeColor(0xFF, 0xFF, 0x00);
                            else
                                color = MakeColor(0xFF, 0x00, 0x00);
                            paintRectNoFill(g, rect, color);
                        }
                    }
                }
            }

            listViewFrames.Refresh();
        }

        private void DoHitboxResize(RSDKv5.Animation.Frame.HitBox hitbox, Point diff) {
            DragPoint which = (DragPoint)resizableHitbox.GetDraggedHandle();

            switch (which) {
                case DragPoint.TopLeft:
                case DragPoint.LeftMiddle:
                case DragPoint.TopMiddle:
                    hitbox.Left += diff.X;
                    hitbox.Top += diff.Y;
                    break;
                case DragPoint.TopRight:
                case DragPoint.RightMiddle:
                    hitbox.Right += diff.X;
                    hitbox.Top += diff.Y;
                    break;
                case DragPoint.BottomLeft:
                    hitbox.Left += diff.X;
                    hitbox.Bottom += diff.Y;
                    break;
                case DragPoint.BottomRight:
                case DragPoint.BottomMiddle:
                    hitbox.Right += diff.X;
                    hitbox.Bottom += diff.Y;
                    break;
                case DragPoint.All:
                    hitbox.Left += diff.X;
                    hitbox.Top += diff.Y;
                    hitbox.Right += diff.X;
                    hitbox.Bottom += diff.Y;
                    break;
                default:
                    break;
            }
        }

        private void panelPreview_MouseDown(object sender, MouseEventArgs e) {
            if (resizableHitbox != null) {
                if (resizableHitbox.CheckHandleDrag(e.Location, panelPreview.Width / 2, panelPreview.Height / 2, zoom)) {
                    UpdateDraggedHandle(e.Location, new Point(0, 0));
                    panelPreview.Refresh();
                }
            }

            previousMousePos = e.Location;
        }

        private bool UpdateDraggedHandle(Point mousePos, Point mouseMovement) {
            bool doRefresh = false;
            int x = panelPreview.Width / 2;
            int y = panelPreview.Height / 2;

            Point diff = resizableHitbox.DoHandleDrag(mouseMovement, x, y, zoom);
            if (diff != Point.Empty)
                doRefresh = true;
            if (resizableHitbox.CheckHandleHighlight(mousePos, x, y, zoom))
                doRefresh = true;

            return doRefresh;
        }

        private void panelPreview_MouseMove(object sender, MouseEventArgs e) {
            Point mousePos = e.Location;
            if (mousePos != previousMousePos && resizableHitbox != null) {
                Point mouseMovement = new Point(mousePos.X - previousMousePos.X, mousePos.Y - previousMousePos.Y);
                if (UpdateDraggedHandle(mousePos, mouseMovement))
                    panelPreview.Refresh();
            }
        }

        private void panelPreview_MouseUp(object sender, MouseEventArgs e) {
            if (resizableHitbox == null || !resizableHitbox.IsDragging())
                return;

            if (!resizableHitbox.DidDrag()) {
                resizableHitbox.FinishHandleDrag();
                return;
            }

            Point diff = resizableHitbox.GetTotalDrag();

            if (listViewFrames.SelectedIndices.Count > 0) {
                int selectedHitbox = comboBoxHitbox.SelectedIndex;
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];
                        DoHitboxResize(frame.HitBoxes[selectedHitbox], diff);
                    }
                }
            }
            else
                DoHitboxResize(GetCurrentHitbox(), diff);

            resizableHitbox.FinishHandleDrag();

            UpdateHitboxNumericUpDownFields(GetCurrentFrame());
            UpdateResizableHitbox();
            SetFileModified();

            panelPreview.Refresh();
        }

        private void listBoxAnimations_DrawItem(object sender, DrawItemEventArgs e) {
            bool isSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            if (e.Index > -1) {
                System.Drawing.Color color = isSelected ? SystemColors.Highlight : listBoxAnimations.BackColor;

                // Background item brush
                SolidBrush backgroundBrush = new SolidBrush(color);
                SolidBrush textBrush = new SolidBrush(e.ForeColor);

                RectangleF rectF = new RectangleF(e.Bounds.X + 2, e.Bounds.Y + 2, e.Bounds.Width, e.Bounds.Height);

                // Draw the background
                e.Graphics.FillRectangle(backgroundBrush, e.Bounds);
                e.Graphics.DrawString(listBoxAnimations.GetItemText(listBoxAnimations.Items[e.Index]), e.Font, textBrush, rectF, StringFormat.GenericDefault);

                // Clean up
                backgroundBrush.Dispose();
                textBrush.Dispose();
            }
            e.DrawFocusRectangle();
        }

        private void listBoxAnimations_SelectedIndexChanged(object sender, EventArgs e) {
            if (noUpdate)
                return;

            listViewFrames.SelectedIndices.Clear();

            currentFrame = 0;
            currentAnimationIndex = listBoxAnimations.SelectedIndex;

            UpdateSheetList();
            UpdateMainPreviewUI();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e) {
            if (AskSaveModified())
                New();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!AskSaveModified())
                return;

            OpenFileDialog svd = new OpenFileDialog();
            svd.RestoreDirectory = true;
            svd.AddExtension = true;
            svd.Filter = "BIN Files (*.bin)|*.bin|All files (*.*)|*.*";
            if (svd.ShowDialog() == DialogResult.OK) {
                Open(svd.FileName);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            Save(false);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            Save(true);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void Form1_ResizeEnd(object sender, EventArgs e) {
            panelPreview.Refresh();
        }

        private void Form1_Resize(object sender, EventArgs e) {
            panelPreview.Refresh();
        }

        private void listView1_DrawItem(object sender, DrawListViewItemEventArgs e) {
            Graphics g = e.Graphics;

            if (currentAnimation.Animations.Count <= 0) return;
            if (!IsAnimIndexValid()) return;

            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;

            RSDKv5.Animation.AnimationEntry animationEntry = GetCurrentAnimation();

            if (animationEntry.Frames.Count > 0 && e.ItemIndex < animationEntry.Frames.Count) {
                RSDKv5.Animation.Frame frame = animationEntry.Frames[e.ItemIndex];

                g.FillRectangle(new SolidBrush(System.Drawing.Color.FromArgb(0xFF, 0x00, 0xFF)), e.Bounds);

                if (spriteSheets.Count > 0) {
                    int w = frame.Width;
                    int h = frame.Height;

                    Bitmap bitmap = spriteSheets[frame.SpriteSheet];

                    if (w > 0 && h > 0 && frame.X < bitmap.Width && frame.Y < bitmap.Height) {
                        int x = e.Bounds.X;
                        int y = e.Bounds.Y;
                        int sw = w;
                        int sh = h;

                        if (w > h) {
                            sw = e.Bounds.Width;
                            sh = h * e.Bounds.Width / w;
                        }
                        else {
                            sw = w * e.Bounds.Height / h;
                            sh = e.Bounds.Height;
                        }

                        x += (e.Bounds.Width - sw) / 2;
                        y += (e.Bounds.Height - sh) / 2;

                        if (sw > 0 && sh > 0)
                            g.DrawImage(bitmap, new Rectangle(x, y, sw, sh), frame.X, frame.Y, w, h, GraphicsUnit.Pixel);
                    }
                }

                System.Drawing.Color c = System.Drawing.Color.FromArgb(0x80, new Pen(SystemBrushes.Highlight).Color);
                if ((e.State & ListViewItemStates.Selected) != 0)
                    g.FillRectangle(new SolidBrush(c), e.Bounds);
            }
        }

        private void SetCurrentFrame(int newFrame) {
            currentFrame = newFrame;
            UpdateFrameUI();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e) {
            if (listViewFrames.SelectedIndices.Count == 0) return;
            if (listViewFrames.SelectedIndices[0] < 0) return;

            currentFrame = listViewFrames.SelectedIndices[0];
            
            if (listViewFrames.SelectedIndices.Count > 1) return;

            UpdateFrameUI();
        }

        private string GetDuplicateAnimName(string newAnimName) {
            int count = 1;
            foreach (RSDKv5.Animation.AnimationEntry an in currentAnimation.Animations) {
                if (an.AnimName.Substring(0, newAnimName.Length < an.AnimName.Length ? newAnimName.Length : an.AnimName.Length) == newAnimName)
                    count++;
            }
            if (count > 1)
                newAnimName += " (" + count + ")";
            return newAnimName;
        } 

        private void toolStripButtonAnimationAdd_Click(object sender, EventArgs e) {
            string newAnimName = GetDuplicateAnimName("New Animation");

            RSDKv5.Animation.AnimationEntry animation = new RSDKv5.Animation.AnimationEntry(newAnimName);

            if (!IsAnimIndexValid()) {
                currentAnimation.Animations.Add(animation);
                currentAnimationIndex = currentAnimation.Animations.Count - 1;
            }
            else {
                currentAnimation.Animations.Insert(currentAnimationIndex + 1, animation);
                currentAnimationIndex++;
            }

            int curAnim = currentAnimationIndex;

            listViewFrames.SelectedIndices.Clear();

            currentFrame = 0;

            UpdateMainPreviewUI();
            UpdateAnimationListUI();

            listBoxAnimations.SelectedIndex = curAnim;

            SetFileModified();
        }

        private void toolStripButtonAnimationDelete_Click(object sender, EventArgs e) {
            if (currentAnimation.Animations.Count == 0) return;
            if (listBoxAnimations.SelectedIndices.Count == 0) return;

            int curAnim = currentAnimationIndex;
            int numSelected = listBoxAnimations.SelectedIndices.Count;

            if (numSelected > 1) {
                for (int i = currentAnimation.Animations.Count; i >= 0; i--) {
                    if (listBoxAnimations.SelectedIndices.Contains(i)) {
                        currentAnimation.Animations.RemoveAt(i);
                        curAnim = i;
                    }
                }
            } else {
                int curIndex = listBoxAnimations.SelectedIndices[0];
                currentAnimation.Animations.RemoveAt(curIndex);
                curAnim = curIndex;
            }

            if (currentAnimation.Animations.Count < 1)
                curAnim = -1;
            else if (curAnim >= currentAnimation.Animations.Count)
                curAnim = currentAnimation.Animations.Count - 1;

            if (numSelected == 1 && curAnim >= 0)
                listBoxAnimations.SetSelected(curAnim, true);
            else
                curAnim = -1;

            currentAnimationIndex = curAnim;

            UpdateMainPreviewUI();
            UpdateAnimationListUI();

            SetFileModified();
        }

        private void DuplicateAnimation(int index) {
            RSDKv5.Animation.AnimationEntry anim = currentAnimation.Animations[index];
            using (MemoryStream output = new MemoryStream()) {
                using (Writer writer = new Writer(output)) {
                    anim.Write(writer, currentAnimation);
                    using (MemoryStream input = new MemoryStream(output.ToArray())) {
                        anim = new RSDKv5.Animation.AnimationEntry(new BinaryReader(input), currentAnimation);
                        anim.AnimName = "Copy of " + anim.AnimName;

                        currentAnimation.Animations.Insert(index + 1, anim);
                        listBoxAnimations.Items.Insert(index + 1, anim.AnimName);

                        SetFileModified();
                    }
                }
            }
        }

        private void toolStripButtonAnimationDuplicate_Click(object sender, EventArgs e) {
            if (currentAnimation.Animations.Count == 0)
                return;

            if (listBoxAnimations.SelectedIndices.Count > 1) {
                List<int> selected = new List<int>();
                List<int> newSelections = new List<int>();

                for (int i = currentAnimation.Animations.Count - 1; i >= 0; i--) {
                    if (listBoxAnimations.SelectedIndices.Contains(i))
                        selected.Add(i);
                }

                for (int i = 0; i < selected.Count; i++) {
                    int index = selected[i];
                    DuplicateAnimation(index);

                    for (int j = 0; j < newSelections.Count; j++)
                        newSelections[j]++;
                    newSelections.Add(index + 1);
                }

                listBoxAnimations.ClearSelected();

                for (int i = 0; i < newSelections.Count; i++)
                    listBoxAnimations.SetSelected(newSelections[i], true);
            } else {
                int curIndex = listBoxAnimations.SelectedIndices[0];

                DuplicateAnimation(curIndex);
                listBoxAnimations.ClearSelected();

                curIndex++;
                currentAnimationIndex = curIndex;

                listBoxAnimations.SetSelected(curIndex, true);
            }

            UpdateMainPreviewUI();
        }

        private void ListBoxSelectMultiple(List<int> newPositions) {
            noUpdate = true;
            listBoxAnimations.SelectedItems.Clear();
            listBoxAnimations.SelectedIndices.Clear();
            foreach (int g in newPositions) {
                listBoxAnimations.SelectedIndices.Add(g);
            }
            noUpdate = false;
        }

        private void toolStripButtonEntryUp_Click(object sender, EventArgs e) {
            int numAnims = currentAnimation.Animations.Count;
            if (numAnims <= 1) return;

            int numSelected = listBoxAnimations.SelectedIndices.Count;
            if (numSelected <= 0)
                return;
            else if (numSelected > 1 && listBoxAnimations.SelectedIndices.Contains(0))
                return;

            List<int> newPositions = new List<int>();
            for (int i = 0; i < numAnims; i++) {
                if (listBoxAnimations.SelectedIndices.Contains(i)) {
                    var entry = currentAnimation.Animations[i];
                    currentAnimation.Animations.RemoveAt(i);

                    var prevPosition = i - 1;
                    if (prevPosition < 0)
                        prevPosition += numAnims;

                    currentAnimation.Animations.Insert(prevPosition, entry);
                    newPositions.Add(prevPosition);
                }
            }

            ListBoxSelectMultiple(newPositions);

            currentAnimationIndex--;
            if (currentAnimationIndex < 0)
                currentAnimationIndex += numAnims;

            listBoxAnimations.Refresh();

            UpdateAnimationListUI();
            SetFileModified();
        }

        private void toolStripButtonEntryDown_Click(object sender, EventArgs e) {
            int numAnims = currentAnimation.Animations.Count;
            if (numAnims <= 1) return;

            int numSelected = listBoxAnimations.SelectedIndices.Count;
            if (numSelected <= 0)
                return;
            else if (numSelected > 1 && listBoxAnimations.SelectedIndices.Contains(numAnims - 1))
                return;

            List<int> newPositions = new List<int>();
            for (int i = numAnims; i >= 0; i--) {
                if (listBoxAnimations.SelectedIndices.Contains(i)) {
                    var entry = currentAnimation.Animations[i];

                    var nextPosition = i + 1;
                    if (nextPosition >= numAnims)
                        nextPosition -= numAnims;

                    currentAnimation.Animations.RemoveAt(i);

                    currentAnimation.Animations.Insert(nextPosition, entry);
                    newPositions.Add(nextPosition);
                }
            }

            ListBoxSelectMultiple(newPositions);

            currentAnimationIndex++;
            if (currentAnimationIndex >= numAnims)
                currentAnimationIndex -= numAnims;

            listBoxAnimations.Refresh();

            UpdateAnimationListUI();
            SetFileModified();
        }

        private byte[] GetUInt16Array(int num) {
            byte[] bytes = new byte[4];
            bytes[0] = (byte)(num & 0xFF);
            bytes[1] = (byte)((num >> 8) & 0xFF);
            return bytes;
        }

        private void toolStripButtonAnimationCopy_Click(object sender, EventArgs e) {
            if (currentAnimation.Animations.Count == 0) return;
            if (listBoxAnimations.SelectedIndices.Count == 0) return;

            using (MemoryStream output = new MemoryStream()) {
                output.Write(BitConverter.GetBytes(0x494E41), 0, 4);

                List<int> animList = new List<int>();

                for (int i = 0; i < currentAnimation.Animations.Count; i++) {
                    if (listBoxAnimations.SelectedIndices.Contains(i))
                        animList.Add(i);
                }

                int count = animList.Count;

                output.Write(GetUInt16Array(count), 0, 2);

                using (Writer writer = new Writer(output)) {
                    for (int i = 0; i < count; i++) {
                        RSDKv5.Animation.AnimationEntry anim = currentAnimation.Animations[animList[i]]; 
                        anim.Write(writer, currentAnimation);
                    }
                }

                byte[] byteArr = output.ToArray();

                Console.WriteLine("Copied " + count + " animation(s): " + byteArr.Length + " bytes");

                PutOnClipboard(byteArr);
            }
        }

        private void toolStripButtonAnimationPaste_Click(object sender, EventArgs e) {
            byte[] clipboard = GetFromClipboard();
            if (clipboard == null)
                return;

            int addIdx = currentAnimationIndex;
            if (addIdx == -1)
                addIdx = 0;

            if (listBoxAnimations.SelectedIndices.Count > 0) {
                for (int i = currentAnimation.Animations.Count - 1; i >= 0; i--) {
                    if (listBoxAnimations.SelectedIndices.Contains(i)) {
                        addIdx = i;
                        break;
                    }
                }
            }

            using (MemoryStream input = new MemoryStream(clipboard)) {
                if (LittleEndian.ReadUInt32(input) == 0x494E41) {
                    int animCount = LittleEndian.ReadInt16(input);

                    List<int> newSelections = new List<int>();

                    for (int i = animCount - 1; i >= 0; i--) {
                        RSDKv5.Animation.AnimationEntry anim = new RSDKv5.Animation.AnimationEntry(new BinaryReader(input), currentAnimation);

                        anim.AnimName = GetDuplicateAnimName("Copy of " + anim.AnimName);

                        currentAnimation.Animations.Insert(addIdx + 1, anim);
                        listBoxAnimations.Items.Insert(addIdx + 1, anim.AnimName);
                        newSelections.Add(addIdx + 1);
                        addIdx++;
                    }

                    currentAnimationIndex = addIdx;

                    listBoxAnimations.ClearSelected();
                    for (int i = 0; i < newSelections.Count; i++)
                        listBoxAnimations.SetSelected(newSelections[i], true);

                    SetFileModified();
                }
            }

            UpdatePreviewUI();
            UpdateFrameButtonsDock();
        }

        private void hitboxEditorToolStripMenuItem_Click(object sender, EventArgs e) {
            HitboxEditor hitboxEditor = new HitboxEditor();

            for (int i = 0; i < currentAnimation.CollisionBoxes.Count; i++) {
                hitboxEditor.listBoxHitboxes.Items.Add(currentAnimation.CollisionBoxes[i]);
                hitboxEditor.NewHitboxPos.Add(i);
            }

            if (hitboxEditor.ShowDialog() == DialogResult.OK) {
                if (comboBoxHitbox.Items.Count > 0)
                    comboBoxHitbox.SelectedIndex = 0;

                if (hitboxEditor.Modified) {
                    currentAnimation.CollisionBoxes.Clear();
                    for (int i = 0; i < hitboxEditor.listBoxHitboxes.Items.Count; i++)
                        currentAnimation.CollisionBoxes.Add((string)hitboxEditor.listBoxHitboxes.Items[i]);

                    if (hitboxEditor.Reorder)
                        ReorderHitboxes(hitboxEditor.NewHitboxPos);

                    for (int a = 0; a < currentAnimation.Animations.Count; a++) {
                        for (int f = 0; f < currentAnimation.Animations[a].Frames.Count; f++) {
                            for (int h = currentAnimation.Animations[a].Frames[f].HitBoxes.Count; h < currentAnimation.CollisionBoxes.Count; h++)
                                currentAnimation.Animations[a].Frames[f].HitBoxes.Add(new RSDKv5.Animation.Frame.HitBox());
                        }
                    }

                    UpdateHitboxList();
                    UpdateMainPreviewUI();
                    SetFileModified();
                }

                noUpdate = true;

                numericUpDownHitboxLeft.Enabled =
                numericUpDownHitboxRight.Enabled =
                numericUpDownHitboxTop.Enabled =
                numericUpDownHitboxBottom.Enabled = currentAnimation.CollisionBoxes.Count != 0;

                noUpdate = false;
            }
        }

        private void ReorderHitboxes(List<int> newHitboxPos) {
            for (int i = 0; i < currentAnimation.Animations.Count; i++) {
                RSDKv5.Animation.AnimationEntry an = currentAnimation.Animations[i];

                for (int j = 0; j < an.Frames.Count; j++) {
                    List<RSDKv5.Animation.Frame.HitBox> boxes = new List<RSDKv5.Animation.Frame.HitBox>();
                    RSDKv5.Animation.Frame frame = an.Frames[j];

                    for (int k = 0; k < frame.HitBoxes.Count; k++)
                        boxes.Add(frame.HitBoxes[k]);

                    frame.HitBoxes.Clear();

                    for (int k = 0; k < boxes.Count; k++) {
                        int newPosition = newHitboxPos[k];
                        if (newPosition >= 0)
                            frame.HitBoxes.Insert(newPosition, boxes[k]);
                    }
                }
            }
        }

        // Animation Properties
        enum AnimationProperty {
            Speed,
            LoopIndex,
            RotationMode
        }

        private void numericUpDownSpeed_ValueChanged(object sender, EventArgs e) {
            SetAnimationProperty(AnimationProperty.Speed, (int)numericUpDownSpeed.Value);
        }
        private void numericUpDownLoopIndex_ValueChanged(object sender, EventArgs e) {
            SetAnimationProperty(AnimationProperty.LoopIndex, (int)numericUpDownLoopIndex.Value);
        }
        private void comboBoxFlags_SelectedIndexChanged(object sender, EventArgs e) {
            SetAnimationProperty(AnimationProperty.RotationMode, (int)comboBoxFlags.SelectedIndex);
        }
        private void SetAnimationProperty(AnimationProperty id, int value) {
            if (!IsAnimIndexValid()) return;
            RSDKv5.Animation.AnimationEntry animationEntry = GetCurrentAnimation();

            bool modified = false;
            switch (id) {
                case AnimationProperty.Speed:
                    modified = animationEntry.FrameSpeed != value;
                    animationEntry.FrameSpeed = value;
                    break;
                case AnimationProperty.LoopIndex:
                    modified = animationEntry.FrameLoop != value;
                    animationEntry.FrameLoop = value;
                    break;
                case AnimationProperty.RotationMode:
                    modified = animationEntry.Unknown != (byte)value;
                    animationEntry.Unknown = (byte)value;
                    break;
            }

            if (modified)
                SetFileModified();
        }
        
        // Comboboxes
        private void comboBoxTexture_SelectedIndexChanged(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;
            if (currentAnimation.SpriteSheets.Count == 0) return;

            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];
                        if (frame.SpriteSheet != comboBoxTexture.SelectedIndex)
                            SetFileModified();
                        frame.SpriteSheet = comboBoxTexture.SelectedIndex;
                    }
                }
            }
            panelPreview.Refresh();
        }
        private void ClearHitboxCombo() {
            noUpdate = true;
            numericUpDownHitboxLeft.Enabled = numericUpDownHitboxRight.Enabled = numericUpDownHitboxTop.Enabled = numericUpDownHitboxBottom.Enabled = false;
            numericUpDownHitboxLeft.Value = numericUpDownHitboxRight.Value = numericUpDownHitboxTop.Value = numericUpDownHitboxBottom.Value = 0;
            noUpdate = false;
        }
        private void comboBoxHitbox_SelectedIndexChanged(object sender, EventArgs e) {
            if (!IsAnimIndexValid() || currentAnimation.Animations.Count == 0) {
                ClearHitboxCombo();
                return;
            }

            RSDKv5.Animation.Frame frame = GetCurrentFrame();
            if (frame == null || frame.HitBoxes.Count == 0) {
                ClearHitboxCombo();
                return;
            }

            UpdateHitboxNumericUpDownFields(frame);
            UpdateResizableHitbox();
            panelPreview.Refresh();
        }

        private void UpdateHitboxNumericUpDownFields(RSDKv5.Animation.Frame frame) {
            if (frame == null)
                return;

            noUpdate = true;
            numericUpDownHitboxLeft.Enabled = numericUpDownHitboxRight.Enabled = numericUpDownHitboxTop.Enabled = numericUpDownHitboxBottom.Enabled = true;

            numericUpDownHitboxTop.Value = frame.HitBoxes[comboBoxHitbox.SelectedIndex].Top;
            numericUpDownHitboxLeft.Value = frame.HitBoxes[comboBoxHitbox.SelectedIndex].Left;
            numericUpDownHitboxRight.Value = frame.HitBoxes[comboBoxHitbox.SelectedIndex].Right;
            numericUpDownHitboxBottom.Value = frame.HitBoxes[comboBoxHitbox.SelectedIndex].Bottom;
            noUpdate = false;
        }

        // Hitbox Rectangle
        private void numericUpDownHitboxLeft_ValueChanged(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;
            if (comboBoxHitbox.SelectedIndex < 0) return;

            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];
                        if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Left != (int)numericUpDownHitboxLeft.Value)
                            SetFileModified();
                        frame.HitBoxes[comboBoxHitbox.SelectedIndex].Left = (int)numericUpDownHitboxLeft.Value;
                        if (!noUpdate) {
                            UpdateResizableHitbox();
                            panelPreview.Refresh();
                        }
                    }
                }
            }
            else if (IsFrameIndexValid()) {
                RSDKv5.Animation.Frame frame = GetCurrentFrame();
                if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Left != (int)numericUpDownHitboxLeft.Value)
                    SetFileModified();
                frame.HitBoxes[comboBoxHitbox.SelectedIndex].Left = (int)numericUpDownHitboxLeft.Value;
                if (!noUpdate) {
                    UpdateResizableHitbox();
                    panelPreview.Refresh();
                }
            }
        }
        private void numericUpDownHitboxTop_ValueChanged(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;
            if (comboBoxHitbox.SelectedIndex < 0) return;

            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];
                        if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Top != (int)numericUpDownHitboxTop.Value)
                            SetFileModified();
                        frame.HitBoxes[comboBoxHitbox.SelectedIndex].Top = (int)numericUpDownHitboxTop.Value;
                        if (!noUpdate) {
                            UpdateResizableHitbox();
                            panelPreview.Refresh();
                        }
                    }
                }
            }
            else if (IsFrameIndexValid()) {
                RSDKv5.Animation.Frame frame = GetCurrentFrame();
                if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Top != (int)numericUpDownHitboxTop.Value)
                    SetFileModified();
                frame.HitBoxes[comboBoxHitbox.SelectedIndex].Top = (int)numericUpDownHitboxTop.Value;

                if (!noUpdate) {
                    UpdateResizableHitbox();
                    panelPreview.Refresh();
                }
            }
        }
        private void numericUpDownHitboxRight_ValueChanged(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;
            if (comboBoxHitbox.SelectedIndex < 0) return;

            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];
                        if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Right != (int)numericUpDownHitboxRight.Value)
                            SetFileModified();
                        frame.HitBoxes[comboBoxHitbox.SelectedIndex].Right = (int)numericUpDownHitboxRight.Value;
                        if (!noUpdate) {
                            UpdateResizableHitbox();
                            panelPreview.Refresh();
                        }
                    }
                }
            }
            else if (IsFrameIndexValid()) {
                RSDKv5.Animation.Frame frame = GetCurrentFrame();
                if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Right != (int)numericUpDownHitboxRight.Value)
                    SetFileModified();
                frame.HitBoxes[comboBoxHitbox.SelectedIndex].Right = (int)numericUpDownHitboxRight.Value;
                if (!noUpdate) {
                    UpdateResizableHitbox();
                    panelPreview.Refresh();
                }
            }
        }
        private void numericUpDownHitboxBottom_ValueChanged(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;
            if (comboBoxHitbox.SelectedIndex < 0) return;

            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];
                        if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Bottom != (int)numericUpDownHitboxBottom.Value)
                            SetFileModified();
                        frame.HitBoxes[comboBoxHitbox.SelectedIndex].Bottom = (int)numericUpDownHitboxBottom.Value;
                        if (!noUpdate) {
                            UpdateResizableHitbox();
                            panelPreview.Refresh();
                        }
                    }
                }
            }
            else if (IsFrameIndexValid()) {
                RSDKv5.Animation.Frame frame = GetCurrentFrame();
                if (frame.HitBoxes[comboBoxHitbox.SelectedIndex].Bottom != (int)numericUpDownHitboxBottom.Value)
                    SetFileModified();
                frame.HitBoxes[comboBoxHitbox.SelectedIndex].Bottom = (int)numericUpDownHitboxBottom.Value;
                if (!noUpdate) {
                    UpdateResizableHitbox();
                    panelPreview.Refresh();
                }
            }
        }

        // Frame Properties
        enum FrameProperty {
            Left,
            Top,
            Width,
            Height,
            CenterX,
            CenterY,
            ID,
            Duration,
        }
        private void numericUpDownLeft_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.Left, (int)numericUpDownLeft.Value, false);
        }
        private void numericUpDownTop_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.Top, (int)numericUpDownTop.Value, false);
        }
        private void numericUpDownWidth_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.Width, (int)numericUpDownWidth.Value, false);
        }
        private void numericUpDownHeight_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.Height, (int)numericUpDownHeight.Value, false);
        }
        private void numericUpDownCenterX_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.CenterX, (int)numericUpDownCenterX.Value, false, false);
        }
        private void numericUpDownCenterY_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.CenterY, (int)numericUpDownCenterY.Value, false, false);
        }
        private void numericUpDownID_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.ID, (int)numericUpDownID.Value, false);
        }
        private void numericUpDownDuration_ValueChanged(object sender, EventArgs e) {
            SetFrameValue(FrameProperty.Duration, (int)numericUpDownDuration.Value, false);
        }
        private void SetFrameValue(FrameProperty id, int value, bool solo, bool absolute = true) {
            if (preventChange) return;
            if (!IsFrameIndexValid()) return;

            bool modified = false;

            if (!absolute) {
                RSDKv5.Animation.Frame frame = GetCurrentFrame();
                switch (id) {
                    case FrameProperty.Left:
                        value -= frame.X;
                        break;
                    case FrameProperty.Top:
                        value -= frame.Y;
                        break;
                    case FrameProperty.Width:
                        value -= frame.Width;
                        break;
                    case FrameProperty.Height:
                        value -= frame.Height;
                        break;
                    case FrameProperty.CenterX:
                        value -= frame.CenterX;
                        break;
                    case FrameProperty.CenterY:
                        value -= frame.CenterY;
                        break;
                    case FrameProperty.ID:
                        value -= frame.ID;
                        break;
                    case FrameProperty.Duration:
                        value -= frame.Duration;
                        break;
                }
            }

            if (listViewFrames.SelectedIndices.Count > 1 && !solo) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();

                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame frame = anim.Frames[i];

                        if (absolute) {
                            switch (id) {
                                case FrameProperty.Left:
                                    modified = (frame.X != value);
                                    frame.X = value;
                                    break;
                                case FrameProperty.Top:
                                    modified = (frame.Y != value);
                                    frame.Y = value;
                                    break;
                                case FrameProperty.Width:
                                    modified = (frame.Width != value);
                                    frame.Width = value;
                                    break;
                                case FrameProperty.Height:
                                    modified = (frame.Height != value);
                                    frame.Height = value;
                                    break;
                                case FrameProperty.CenterX:
                                    modified = (frame.CenterX != value);
                                    frame.CenterX = value;
                                    break;
                                case FrameProperty.CenterY:
                                    modified = (frame.CenterY != value);
                                    frame.CenterY = value;
                                    break;
                                case FrameProperty.ID:
                                    modified = (frame.ID != value);
                                    frame.ID = value;
                                    break;
                                case FrameProperty.Duration:
                                    modified = (frame.Duration != value);
                                    frame.Duration = value;
                                    break;
                            }
                        }
                        else {
                            modified = (value != 0);

                            switch (id) {
                                case FrameProperty.Left:
                                    frame.X += value;
                                    break;
                                case FrameProperty.Top:
                                    frame.Y += value;
                                    break;
                                case FrameProperty.Width:
                                    frame.Width += value;
                                    break;
                                case FrameProperty.Height:
                                    frame.Height += value;
                                    break;
                                case FrameProperty.CenterX:
                                    frame.CenterX += value;
                                    break;
                                case FrameProperty.CenterY:
                                    frame.CenterY += value;
                                    break;
                                case FrameProperty.ID:
                                    frame.ID += value;
                                    break;
                                case FrameProperty.Duration:
                                    frame.Duration += value;
                                    break;
                            }
                        }

                        panelPreview.Refresh();
                    }
                }
            }
            else {
                RSDKv5.Animation.Frame frame = GetCurrentFrame();

                if (absolute) {
                    switch (id) {
                        case FrameProperty.Left:
                            modified = (frame.X != value);
                            frame.X = value;
                            break;
                        case FrameProperty.Top:
                            modified = (frame.Y != value);
                            frame.Y = value;
                            break;
                        case FrameProperty.Width:
                            modified = (frame.Width != value);
                            frame.Width = value;
                            break;
                        case FrameProperty.Height:
                            modified = (frame.Height != value);
                            frame.Height = value;
                            break;
                        case FrameProperty.CenterX:
                            modified = (frame.CenterX != value);
                            frame.CenterX = value;
                            break;
                        case FrameProperty.CenterY:
                            modified = (frame.CenterY != value);
                            frame.CenterY = value;
                            break;
                        case FrameProperty.ID:
                            modified = (frame.ID != value);
                            frame.ID = value;
                            break;
                        case FrameProperty.Duration:
                            modified = (frame.Duration != value);
                            frame.Duration = value;
                            break;
                    }
                }
                else {
                    modified = (value != 0);

                    switch (id) {
                        case FrameProperty.Left:
                            frame.X += value;
                            break;
                        case FrameProperty.Top:
                            frame.Y += value;
                            break;
                        case FrameProperty.Width:
                            frame.Width += value;
                            break;
                        case FrameProperty.Height:
                            frame.Height += value;
                            break;
                        case FrameProperty.CenterX:
                            frame.CenterX += value;
                            break;
                        case FrameProperty.CenterY:
                            frame.CenterY += value;
                            break;
                        case FrameProperty.ID:
                            frame.ID += value;
                            break;
                        case FrameProperty.Duration:
                            frame.Duration += value;
                            break;
                    }
                }

                panelPreview.Refresh();

                if (modified)
                    SetFileModified();
            }
        }

        private void listBoxAnimations_DoubleClick(object sender, EventArgs e) {
            if (!IsAnimIndexValid())
                return;

            RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
            PromptDialog prompt = new PromptDialog("New name for \"" + anim.AnimName + "\"?", anim.AnimName);
            if (prompt.ShowDialog() != DialogResult.OK)
                return;

            anim.AnimName = prompt.TextEntry.Text;
            if (!String.Equals(listBoxAnimations.Items[currentAnimationIndex], anim.AnimName))
                SetFileModified();
            listBoxAnimations.Items[currentAnimationIndex] = anim.AnimName;
        }

        private void toolStripButtonFrameAdd_Click(object sender, EventArgs e) {
            if (!IsAnimIndexValid())
                return;

            RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
            RSDKv5.Animation.Frame f = new RSDKv5.Animation.Frame(32, 32);
            f.Anim = currentAnimation;
            for (int i = 0; i < currentAnimation.CollisionBoxes.Count; ++i) {
                var hitBox = new RSDKv5.Animation.Frame.HitBox();
                hitBox.Left = 0;
                hitBox.Top = 0;
                hitBox.Right = 0;
                hitBox.Bottom = 0;
                f.HitBoxes.Add(hitBox);
            }
            anim.Frames.Add(f);
            listViewFrames.Items.Add("");
            SetFileModified();
        }

        private void toolStripButtonFrameDelete_Click(object sender, EventArgs e) {
            if (IsAnimIndexValid() && listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();

                bool deletedAny = false;
                for (int i = anim.Frames.Count - 1; i >= 0; i--) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        anim.Frames.RemoveAt(i);
                        listViewFrames.Items.RemoveAt(i);
                        deletedAny = true;
                    }
                }

                if (deletedAny) {
                    if (currentFrame > anim.Frames.Count - 1)
                        currentFrame = anim.Frames.Count - 1;

                    UpdateMainPreviewUI();
                    SetFileModified();
                }
            }
        }

        private static DialogResult ShowInputDialog(ref int input, string title, int min, int max) {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = title;

            inputBox.StartPosition = FormStartPosition.CenterParent;

            System.Windows.Forms.NumericUpDown textBox = new NumericUpDown();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            if (min != 0 && max != 0) {
                textBox.Minimum = min;
                textBox.Maximum = max;
            }
            else {
                textBox.Minimum = -2147483648;
                textBox.Maximum = 2147483647;
            }
            textBox.Value = input;
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = (int)textBox.Value;
            return result;
        }

        private void toolStripButtonFrameDuplicate_Click(object sender, EventArgs e) {
            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                using (MemoryStream output = new MemoryStream()) {
                    int whereEnd = anim.Frames.Count;
                    for (int i = 0; i < whereEnd; i++) {
                        if (listViewFrames.SelectedIndices.Contains(i)) {
                            anim.Frames.Insert(i + 1, anim.Frames[i].Clone());
                            listViewFrames.Items.Add("");
                            SetFileModified();
                        }
                    }
                }
            }
        }
        private void toolStripButtonFrameDuplicate_AltClick(object sender, EventArgs e) {
            if (listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();

                int deltaX = 0;
                int deltaY = 0;
                int count = 1;
                if (ShowInputDialog(ref deltaX, "Delta X", -256, 256) != DialogResult.OK)
                    return;
                if (ShowInputDialog(ref deltaY, "Delta Y", -256, 256) != DialogResult.OK)
                    return;
                if (ShowInputDialog(ref count, "Copies?", 1, 256) != DialogResult.OK)
                    return;

                using (MemoryStream output = new MemoryStream()) {
                    int whereEnd = anim.Frames.Count;
                    int i = listViewFrames.SelectedIndices[0];
                    for (int c = 0; c < count; c++) {
                        var frame = anim.Frames[i].Clone();
                        frame.X += deltaX;
                        frame.Y += deltaY;
                        anim.Frames.Insert(i + 1, frame);
                        i++;
                        listViewFrames.Items.Add("");
                        SetFileModified();
                    }
                }
            }
        }

        private void toolStripButtonFrameLeft_Click(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;

            if (listViewFrames.SelectedIndices.Count > 0) {
                if (listViewFrames.SelectedIndices.Contains(0)) return;

                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();

                List<int> guys = new List<int>();
                for (int i = 0; i < anim.Frames.Count; i++) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame f = anim.Frames[i];
                        anim.Frames.RemoveAt(i);
                        anim.Frames.Insert(i - 1, f);
                        guys.Add(i - 1);
                    }
                }
                
                listViewFrames.SelectedItems.Clear();
                listViewFrames.SelectedIndices.Clear();
                foreach (int g in guys) {
                    listViewFrames.SelectedIndices.Add(g);
                }

                currentFrame--;
                if (currentFrame < 0)
                    currentFrame = 0;

                listViewFrames.Refresh();
                SetFileModified();
            }
        }

        private void toolStripButtonFrameRight_Click(object sender, EventArgs e) {
            if (!IsAnimIndexValid()) return;

            RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();

            if (listViewFrames.SelectedIndices.Count > 0) {
                if (listViewFrames.SelectedIndices.Contains(anim.Frames.Count - 1)) return;

                List<int> guys = new List<int>();
                for (int i = anim.Frames.Count - 1; i >= 0; i--) {
                    if (listViewFrames.SelectedIndices.Contains(i)) {
                        RSDKv5.Animation.Frame f = anim.Frames[i];
                        anim.Frames.RemoveAt(i);
                        anim.Frames.Insert(i + 1, f);
                        guys.Add(i + 1);
                    }
                }

                listViewFrames.SelectedItems.Clear();
                listViewFrames.SelectedIndices.Clear();
                foreach (int g in guys) {
                    listViewFrames.SelectedIndices.Add(g);
                }

                currentFrame++;
                if (currentFrame > anim.Frames.Count - 1)
                    currentFrame = anim.Frames.Count - 1;

                listViewFrames.Refresh();
                SetFileModified();
            }
        }

        private void toolStripButtonFrameCopy_Click(object sender, EventArgs e) {
            if (IsAnimIndexValid() && listViewFrames.SelectedIndices.Count > 0) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                using (MemoryStream output = new MemoryStream()) {
                    using (Writer writer = new Writer(output)) {
                        for (int i = 0; i < anim.Frames.Count; i++) {
                            if (listViewFrames.SelectedIndices.Contains(i)) {
                                output.Write(BitConverter.GetBytes(0x69696969), 0, 4);
                                RSDKv5.Animation.Frame.WriteFrame(writer, currentAnimation, anim, i);
                            }
                        }
                        output.Write(BitConverter.GetBytes(0xFFFFFFFF), 0, 4);
                        PutOnClipboard(output.ToArray());
                    }
                }
            }
        }

        private void toolStripButtonFramePaste_Click(object sender, EventArgs e) {
            byte[] clipboard = GetFromClipboard();
            if (clipboard == null)
                return;

            if (IsAnimIndexValid()) {
                using (MemoryStream input = new MemoryStream(clipboard)) {
                    if (listViewFrames.SelectedIndices.Count > 0) {
                        RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                        int insertWhere = 0;
                        for (int i = 0; i < anim.Frames.Count; i++) {
                            if (listViewFrames.SelectedIndices.Contains(i)) {
                                if (insertWhere < i + 1)
                                    insertWhere = i + 1;
                            }
                        }
                        while (LittleEndian.ReadUInt32(input) == 0x69696969) {
                            anim.Frames.Insert(insertWhere, RSDKv5.Animation.Frame.ReadFrame(new BinaryReader(input), currentAnimation));
                            listViewFrames.Items.Add("");
                            insertWhere++;
                        }
                    }
                    else {
                        while (LittleEndian.ReadUInt32(input) == 0x69696969) {
                            RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();
                            anim.Frames.Add(RSDKv5.Animation.Frame.ReadFrame(new BinaryReader(input), currentAnimation));
                            listViewFrames.Items.Add("");
                        }
                    }

                    SetFileModified();
                }
            }
        }

        private void toolStripButtonOffsetFrames_Click(object sender, EventArgs e) {
            if (IsAnimIndexValid()) {
                RSDKv5.Animation.AnimationEntry anim = GetCurrentAnimation();

                int offsetX = 0;
                int offsetY = 0;
                if (ShowInputDialog(ref offsetX, "X Offset", 0, 0) != DialogResult.OK)
                    return;
                if (ShowInputDialog(ref offsetY, "Y Offset", 0, 0) != DialogResult.OK)
                    return;

                for (int i = 0; i < anim.Frames.Count; i++) {
                    RSDKv5.Animation.Frame f = anim.Frames[i];
                    f.CenterX += offsetX;
                    f.CenterY += offsetY;
                }

                panelPreview.Refresh();

                SetFileModified();
            }
        }

        private void spritesheetListEditorToolStripMenuItem_Click(object sender, EventArgs e) {
            string spritesFolder = "";
            if (currentFilename != "") {
                spritesFolder = PathHelper.GetSpritesFolder(currentFilename);

                if (spritesFolder == "")
                    spritesFolder = Directory.GetParent(currentFilename).FullName;
            } else
                spritesFolder = Directory.GetCurrentDirectory();

            SpritesheetList sheetList = new SpritesheetList();

            for (int i = 0; i < currentAnimation.SpriteSheets.Count; i++) {
                sheetList.listBoxSpritesheetFilenames.Items.Add(currentAnimation.SpriteSheets[i]);
                sheetList.newSheetPos.Add(i);
            }

            sheetList.spritesFolder = spritesFolder;

            if (sheetList.ShowDialog() == DialogResult.OK) {
                int ogIndex = comboBoxTexture.SelectedIndex;
                if (comboBoxTexture.Items.Count > 0)
                    comboBoxTexture.SelectedIndex = 0;

                for (int i = 0; i < spriteSheets.Count; i++) {
                    spriteSheets[i].Dispose();
                }
                spriteSheets.Clear();

                if (sheetList.modified) {
                    currentAnimation.SpriteSheets.Clear();
                    for (int i = 0; i < sheetList.listBoxSpritesheetFilenames.Items.Count; i++) {
                        currentAnimation.SpriteSheets.Add((string)sheetList.listBoxSpritesheetFilenames.Items[i]);
                    }

                    ReorderSheets(sheetList.newSheetPos);
                    SetFileModified();
                }

                spritesFolder = "";
                if (currentFilename != "")
                    spritesFolder = PathHelper.GetSpritesFolder(currentFilename);

                if (!PathHelper.EndsWith(spritesFolder, "Sprites"))
                    spritesFolder = sheetList.spritesFolder;

                if (spritesFolder == "")
                    spritesFolder = Directory.GetParent(currentFilename).FullName;

                UpdatePreviewSheets(spritesFolder);
                UpdateSheetList();
                UpdateMainPreviewUI();

                if (comboBoxTexture.Items.Count > 0) {
                    if (ogIndex < comboBoxTexture.Items.Count)
                        comboBoxTexture.SelectedIndex = ogIndex;
                    else
                        comboBoxTexture.SelectedIndex = 0;
                }
            }
        }

        private void ReorderSheets(List<int> newSheetPos) {
            for (int i = 0; i < currentAnimation.Animations.Count; i++) {
                RSDKv5.Animation.AnimationEntry an = currentAnimation.Animations[i];

                for (int j = 0; j < an.Frames.Count; j++) {
                    RSDKv5.Animation.Frame frame = an.Frames[j];
                    int newSheet = newSheetPos[frame.SpriteSheet];

                    if (newSheet < 0)
                        newSheet = 0;

                    frame.SpriteSheet = (byte)newSheet;
                }
            }
        }

        private void ShowAseExportDialog(string[] asefiles) {
            using (SaveFileDialog sfd = new SaveFileDialog()) {
                sfd.RestoreDirectory = true;
                sfd.AddExtension = true;
                sfd.Filter = "BIN Files (*.bin)|*.bin|All files (*.*)|*.*";
                sfd.Title = "Location to save the animation files...";
                if (sfd.ShowDialog() == DialogResult.OK) {
                    New();
                    ASEtoRSDK_Convert(asefiles, sfd.FileName);
                }
            }
        }

        private void importAsepriteToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!AskSaveModified())
                return;

            using (OpenFileDialog ofd = new OpenFileDialog()) {
                ofd.RestoreDirectory = true;
                ofd.AddExtension = true;
                ofd.Multiselect = true;
                ofd.Filter = "Aseprite Files (*.ase,*.aseprite)|*.ase;*.aseprite|All files (*.*)|*.*";
                ofd.Title = "Open Aseprite file...";
                if (ofd.ShowDialog() == DialogResult.OK) {
                    ShowAseExportDialog(ofd.FileNames);
                }
            }
        }

        private void importGIFToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e) {
            if (zoom < 10) {
                zoom++;
                UpdateMainPreviewUI();
            }
        }

        private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e) {
            if (zoom > 1) {
                zoom--;
                UpdateMainPreviewUI();
            }
        }

        private void resetZoomToolStripMenuItem_Click(object sender, EventArgs e) {
            zoom = 1;
            UpdateMainPreviewUI();
        }

        private void showBoundsToolStripMenuItem_Click(object sender, EventArgs e) {
            showBounds = showBoundsToolStripMenuItem.Checked;
            panelPreview.Refresh();
        }

        private void showHitboxToolStripMenuItem_Click(object sender, EventArgs e) {
            showHitbox = showHitboxToolStripMenuItem.Checked;
            UpdateResizableHitbox();
            panelPreview.Refresh();
        }

        private void showAnimationIDsToolStripMenuItem_Click(object sender, EventArgs e) {
            showEntryID = showAnimationIDsToolStripMenuItem.Checked;
            UpdateAnimationListUI();
        }

        private void AnimationEditor_DragEnter(object sender, DragEventArgs e) {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        private void AnimationEditor_DragDrop(object sender, DragEventArgs e) {
            if (!AskSaveModified())
                return;

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length < 1)
                return;

            bool doAseExport = false;

            for (var i = 0; i < files.Length; i++) {
                string extension = Path.GetExtension(files[i]);
                if (extension == ".ase" || extension == ".aseprite") {
                    doAseExport = true;
                    break;
                }
            }

            if (doAseExport)
                ShowAseExportDialog(files);
            else
                Open(files[0]);
        }

        private void AnimationEditor_Closing(object sender, CancelEventArgs e) {
            if (!AskSaveModified())
                e.Cancel = true;
        }

        private void listBoxAnimations_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = true;
        }
    }
}
