﻿namespace HatchAnimEditor {
    public class Jenkins {
        public static uint HashUInt32(uint key, uint hash) {
            hash += key;
            hash += hash << 10;
            hash ^= hash >> 6;

            hash += hash << 3;
            hash ^= hash >> 11;
            hash += hash << 15;
            return hash;
        }
    }
}
