using System;
using System.Drawing;

namespace HatchAnimEditor {
    public enum DragPoint : uint {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        TopMiddle,
        RightMiddle,
        BottomMiddle,
        LeftMiddle,
        All
    }

    public class ResizableHitbox {
        private Rectangle Rect;
        private Rectangle DragRect;
        private int PointDragged = -1;
        private int PointHighlighted = -1;
        private Point TotalDrag;
        private Point[] Points = new Point[8];
        private Rectangle[] Handles = new Rectangle[8];
        private Rectangle[] PanelHandles = new Rectangle[8];
        private const int HANDLE_SIZE = 16;

        public ResizableHitbox(Rectangle rect) {
            Update(rect);
        }

        public void Update(Rectangle rect) {
            Rect = rect;

            for (int i = 0; i < 8; i++) {
                Point p = GetPointPos(Rect, (DragPoint)i);
                Handles[i] = MakeHandle(p);
                Points[i] = p;
            }
        }

        public Rectangle MakeHandle(Point p) {
            return new Rectangle(p.X - HANDLE_SIZE/2, p.Y - HANDLE_SIZE/2, HANDLE_SIZE, HANDLE_SIZE);
        }

        public int CheckHandleTouch(Point location, int panelX, int panelY, int panelZoom) {
            for (int i = 0; i < 8; i++) {
                if (GetPanelHandle((DragPoint)i, panelX, panelY, panelZoom).Contains(location))
                    return i;
            }

            if (GetPanelHitbox(panelX, panelY, panelZoom).Contains(location))
                return (int)DragPoint.All;

            return -1;
        }
        public bool CheckHandleDrag(Point location, int panelX, int panelY, int panelZoom) {
            int which = CheckHandleTouch(location, panelX, panelY, panelZoom);
            if (which == -1)
                return false;

            PointDragged = which;
            return true;
        }
        public bool CheckHandleHighlight(Point location, int panelX, int panelY, int panelZoom) {
            int last = PointHighlighted;

            if (PointDragged != -1)
                PointHighlighted = PointDragged;
            else
                PointHighlighted = CheckHandleTouch(location, panelX, panelY, panelZoom);

            return last != PointHighlighted;
        }
        public Point DoHandleDrag(Point mouseMovement, int panelX, int panelY, int panelZoom) {
            if (PointDragged == -1)
                return Point.Empty;

            int diffX = 0;
            int diffY = 0;

            switch ((DragPoint)PointDragged) {
                case DragPoint.TopLeft:
                case DragPoint.TopRight:
                case DragPoint.BottomLeft:
                case DragPoint.BottomRight:
                case DragPoint.All:
                    diffX = mouseMovement.X / panelZoom;
                    diffY = mouseMovement.Y / panelZoom;
                    break;
                case DragPoint.TopMiddle:
                case DragPoint.BottomMiddle:
                    diffY = mouseMovement.Y / panelZoom;
                    break;
                case DragPoint.RightMiddle:
                case DragPoint.LeftMiddle:
                    diffX = mouseMovement.X / panelZoom;
                    break;
                default:
                    return Point.Empty;
            }

            TotalDrag = new Point(diffX, diffY);

            UpdateDraggedHandle(panelX, panelY, panelZoom);

            return TotalDrag;
        }
        public Point GetTotalDrag() {
            return TotalDrag;
        }
        public void FinishHandleDrag() {
            PointDragged = -1;
        }

        public bool IsDragging() {
            return PointDragged != -1;
        }
        public bool DidDrag() {
            return TotalDrag.X != 0 || TotalDrag.Y != 0;
        }
        public int GetDraggedHandle() {
            return PointDragged;
        }
        public int GetHighlightedHandle() {
            return PointHighlighted;
        }

        public Rectangle GetPanelRect(Rectangle rect, int x, int y, int zoom) {
            return new Rectangle(x + rect.Left * zoom, y + rect.Top * zoom, rect.Width * zoom, rect.Height * zoom);
        }
        public Rectangle GetPanelHitbox(int x, int y, int zoom) {
            if (PointDragged != -1)
                return DragRect;

            return GetPanelRect(Rect, x, y, zoom);
        }
        public Rectangle GetPanelHandle(DragPoint which, int x, int y, int zoom) {
            if (PointDragged != -1)
                return PanelHandles[(int)which];

            return GetPanelRect(Handles[(int)which], x, y, zoom);
        }

        public void UpdateDraggedHandle(int panelX, int panelY, int panelZoom) {
            if (PointDragged == -1)
                return;

            int left = panelX + Rect.Left * panelZoom;
            int top = panelY + Rect.Top * panelZoom;
            int right = panelX + Rect.Right * panelZoom;
            int bottom = panelY + Rect.Bottom * panelZoom;

            switch ((DragPoint)PointDragged) {
                case DragPoint.TopLeft:
                case DragPoint.LeftMiddle:
                case DragPoint.TopMiddle:
                    left += TotalDrag.X;
                    top += TotalDrag.Y;
                    break;
                case DragPoint.TopRight:
                case DragPoint.RightMiddle:
                    right += TotalDrag.X;
                    top += TotalDrag.Y;
                    break;
                case DragPoint.BottomLeft:
                    left += TotalDrag.X;
                    bottom += TotalDrag.Y;
                    break;
                case DragPoint.BottomRight:
                case DragPoint.BottomMiddle:
                    right += TotalDrag.X;
                    bottom += TotalDrag.Y;
                    break;
                case DragPoint.All:
                    left += TotalDrag.X;
                    top += TotalDrag.Y;
                    right += TotalDrag.X;
                    bottom += TotalDrag.Y;
                    break;
                default:
                    break;
            }

            DragRect = new Rectangle(left, top, right - left, bottom - top);

            for (int i = 0; i < 8; i++) {
                Point p = GetPointPos(DragRect, (DragPoint)i);
                PanelHandles[i] = MakeHandle(p);
            }
        }

        private Point GetPointPos(Rectangle rect, DragPoint which) {
            switch (which) {
                case DragPoint.TopLeft:
                    return new Point(rect.Left, rect.Top);
                case DragPoint.TopRight:
                    return new Point(rect.Right, rect.Top);
                case DragPoint.BottomLeft:
                    return new Point(rect.Left, rect.Bottom);
                case DragPoint.BottomRight:
                    return new Point(rect.Right, rect.Bottom);
                case DragPoint.TopMiddle:
                    return new Point(rect.Left + (rect.Width / 2), rect.Top);
                case DragPoint.RightMiddle:
                    return new Point(rect.Right, rect.Top + (rect.Height / 2));
                case DragPoint.BottomMiddle:
                    return new Point(rect.Left + (rect.Width / 2), rect.Bottom);
                case DragPoint.LeftMiddle:
                    return new Point(rect.Left, rect.Top + (rect.Height / 2));
                default:
                    return Point.Empty;
            }
        }
    }
}
