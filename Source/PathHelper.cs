﻿using System;
using System.IO;

namespace HatchAnimEditor {
    public static class PathHelper {
        public static bool EndsWith(string path, string endsWith) {
            if (path == "")
                return false;

            if (path.EndsWith(endsWith))
                return true;
            else if (path.EndsWith(endsWith + Path.DirectorySeparatorChar))
                return true;
            else if (path.EndsWith(endsWith + '\\'))
                return true;

            return false;
        }

        public static bool HasSeparator(string path) {
            if (path == "")
                return false;

            char lastChar = path[path.Length - 1];
            return (lastChar == Path.DirectorySeparatorChar || lastChar == '\\');
        }

        public static string GetSpritesFolder(string path) {
            if (path == "")
                return path;

            do {
                path = Directory.GetParent(path).FullName;
            }
            while (path.Length > 14 && !EndsWith(path, "Sprites"));

            return path;
        }
    }
}
